import React, { useEffect, useState } from 'react'
import { PermissionsAndroid } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import messaging from '@react-native-firebase/messaging'

import { getProfile, notificationRegister } from './app/api/auth'

import { UserContext, initialUserContextState } from './app/context/userContext'
import { LocationContext, initialLocationContextState } from './app/context/locationContext'
import { CollectionsContext, initialCollectionsContextState } from './app/context/collectionsContext'

import Linking from './app/utils/Linking'

import Auth from './app/pages/Auth'
import Collection from './app/pages/Collection'
import Settings from './app/pages/Settings'
import Tabs from './app/pages/Tabs/Tabs'
import Eatery from './app/pages/Eatery/Eatery'
import Photos from './app/pages/Eatery/Photos'
import Menu from './app/pages/Eatery/Menu'
import Reviews from './app/pages/Eatery/Reviews'
import Filters from './app/pages/Filters'
import Promotion from './app/pages/Promotion'
import Event from './app/pages/Event'
import NewReview from './app/pages/Eatery/NewReview'
import User from './app/pages/User'
import SaveToCollection from './app/pages/SaveToCollection'

import Colors from './app/constants/Colors'


export default () => {
  const Stack = createNativeStackNavigator()

  const [user, setUser] = useState(initialUserContextState.user)
  const [location, setLocation] = useState(initialLocationContextState.location)
  const [collections, setCollections] = useState(initialCollectionsContextState.collections)

  AsyncStorage.getItem('user').then(async storedUser => {
    if (storedUser && user.Empty === true) {
      await getProfile(JSON.parse(storedUser).At.Token).then(resp => {
        return resp.json()
      }).then(body => {
        setUser({ ...user, ...JSON.parse(storedUser), ...body, Empty: false })
      })
    }
  })

  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission().then(() => 0)
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      //console.log('Authorization status:', authStatus);
    }
  }

  const updateToken = async () => {
    await messaging().getToken().then(async token => {
      if (user.Empty) {
        await notificationRegister({ AccountGUID: null, Token: token })
      } else {
        await notificationRegister({ AccountGUID: user.AccountGUID, Token: token })
      }
    })
  }

  useEffect(() => {
    updateToken()
  }, [user])

  useEffect(() => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS).then(resp => 0);
    requestUserPermission().then(resp => 0)
    messaging().setBackgroundMessageHandler(() => new Promise<void>((resolve) => resolve()))
    AsyncStorage.getItem('city').then(city => {
      if (city === null) {
        AsyncStorage.setItem('city', 'Нижний Новгород').then(() => {
          setUser({ ...user, City: 'Нижний Новгород' })
        })
      } else if (user.Empty) {
        setUser({ ...user, City: city })
      }
    })
  }, [])

  return (
    <NavigationContainer linking={Linking}>
      <LocationContext.Provider value={{ location, setLocation }}>
        <UserContext.Provider value={{ user, setUser }}>
          <CollectionsContext.Provider value={{ collections, setCollections }}>
            <Stack.Navigator screenOptions={{ headerStyle: { backgroundColor: Colors.Background }, navigationBarColor: Colors.Background, contentStyle: { backgroundColor: Colors.Background } }} initialRouteName='Tabs'>
              <Stack.Screen name="Auth" component={Auth} options={{ presentation: 'modal' }} />
              <Stack.Screen name="Filters" component={Filters} options={{ presentation: 'modal' }} />
              <Stack.Screen name="Tabs" component={Tabs} options={{ headerShown: false, navigationBarColor: Colors.Orange0 }} />
              <Stack.Screen name='Eatery' component={Eatery} />
              <Stack.Screen name="EateryPhotos" component={Photos} />
              <Stack.Screen name="EateryMenu" component={Menu} />
              <Stack.Screen name="EateryReviews" component={Reviews} />
              <Stack.Screen name="Settings" component={Settings} />
              <Stack.Screen name="Collection" component={Collection} />
              <Stack.Screen name="Promotion" component={Promotion} />
              <Stack.Screen name="Event" component={Event} />
              <Stack.Screen name="NewReview" component={NewReview} />
              <Stack.Screen name="User" component={User} />
              <Stack.Screen name="SaveToCollection" component={SaveToCollection} />
            </Stack.Navigator>
          </CollectionsContext.Provider>
        </UserContext.Provider>
      </LocationContext.Provider>
    </NavigationContainer>
  )
}
