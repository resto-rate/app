import { createContext, useContext } from "react"

export type Collection = {
    CollectionGUID: string
    Preview: string
    Title: string
    EstablishmentCount: number
    AccountGUID: string
}

type CollectionsContext = {
    collections: { Count: number, Collections: Collection[] }
    setCollections: (collections: { Count: number, Collections: Collection[] }) => void
}

export const initialCollectionsContextState: CollectionsContext = {
    collections: {
        Count: 0,
        Collections: [],
    },
    setCollections: (collections: { Count: number, Collections: Collection[] }) => 0
}

export const CollectionsContext = createContext<CollectionsContext>(initialCollectionsContextState)
export const useCollectionsContext = () => useContext(CollectionsContext)
