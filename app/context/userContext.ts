import { createContext, useContext } from 'react'

export type User = {
    Phone: string
    Name: string
    Surname: string
    City: string
    ImageURL: string
    AccountGUID: string
    At: { Exp: number, Token: string }
    Rt: { Exp: number, Token: string }
    LinkObjectGUID: string
    Empty: boolean
}

type UserContext = {
    user: User,
    setUser: (user: User) => void
}

export const initialUserContextState: UserContext = {
    user: {
        Phone: '',
        Name: '',
        Surname: '',
        City: '',
        ImageURL: '',
        AccountGUID: '',
        At: { Exp: 0, Token: '' },
        Rt: { Exp: 0, Token: '' },
        LinkObjectGUID: '',
        Empty: true,
    },
    setUser: (user: User) => {
    }
}

export const UserContext = createContext<UserContext>(initialUserContextState)
export const useUserContext = () => useContext(UserContext)
