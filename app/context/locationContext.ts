import { createContext, useContext } from "react"

type Location = {
    City: string
}

type LocationContext = {
    location: Location
    setLocation: (location: Location) => void
}

export const initialLocationContextState: LocationContext = {
    location: {
        City: 'Нижний Новгород',
    },
    setLocation: (location: Location) => {

    }
}

export const LocationContext = createContext<LocationContext>(initialLocationContextState)
export const useLocationContext = () => useContext(LocationContext)
