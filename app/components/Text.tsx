import React from 'react'
import { StyleSheet, Text as ReactText } from 'react-native'

import Colors from '../constants/Colors'


type TextProps = {
    children: any
    type?: 'h1' | 'h2' | 'h3' | 'default' | 'sec1' | 'sec2'
    state?: 'primary' | 'secondary' | 'disabled' | 'active' | 'contrast' | 'error' | 'placeholder'
    style?: any
    numberOfLines?: number
}

export default ({ children, type = 'default', state = 'primary', style = {}, numberOfLines = 0 }: TextProps) => {
    let customStyles = [styles[type], styles[state], style]
    if (['200', '300', '400', '500', '600', '700', '800'].includes(style.fontWeight)) {
        style.fontFamily = `Manrope${style.fontWeight}`
    }

    return <ReactText style={customStyles} numberOfLines={numberOfLines}>{children}</ReactText>
}

const styles = StyleSheet.create({
    h1: {
        fontFamily: 'Manrope500',
        fontSize: 24,
    },
    h2: {
        fontFamily: 'Manrope600',
        fontSize: 20,
    },
    h3: {
        fontFamily: 'Manrope500',
        fontSize: 18,
    },
    default: {
        fontFamily: 'Manrope400',
        fontSize: 16,
    },
    sec1: {
        fontFamily: 'Manrope400',
        fontSize: 14,
    },
    sec2: {
        fontFamily: 'Manrope400',
        fontSize: 12,
    },
    primary: {
        color: Colors.Base900,
    },
    secondary: {
        color: Colors.Base500,
    },
    disabled: {
        color: Colors.Base400,
    },
    active: {
        color: Colors.Orange800,
    },
    contrast: {
        color: Colors.Base0,
    },
    error: {
        color: Colors.Orange900,
    },
    placeholder: {
        color: Colors.Orange300,
    },
})
