import { View, Modal, TouchableOpacity, StyleSheet } from 'react-native'
import React from 'react'
import Text from './Text'
import Input from './Input'
import Button from './Button'
import Icons from './Icons/Icons'
import Colors from '../constants/Colors'
import { useCollectionsContext } from '../context/collectionsContext'
import { createCollection } from '../api/establishment'
import { useUserContext } from '../context/userContext'
import { EateryCardProps } from './EateryCard'

type Collection = {
    CollectionGUID: string
    AccountGUID: string
    Title: string
    Preview: string
    EstablishmentsCount: 0
    Establishments: EateryCardProps[]
}

type CreateUpdateCollectionPropsType = {
    modalVisible: boolean
    setModalVisible: (modalVisible: boolean) => void
    title: string
    setTitle: (title: string) => void
    collection?: Collection
    setCollection?: (collection: Collection) => void
}

export default (props: CreateUpdateCollectionPropsType) => {
    const { user } = useUserContext()
    const { collections, setCollections } = useCollectionsContext()

    const saveCollection = async () => {
        if (props.collection !== undefined && props.setCollection !== undefined) {
            await createCollection({ Title: props.title, CollectionGUID: props.collection.CollectionGUID }, user.At.Token).then(resp => {
                if (resp.ok) {
                    return resp.json()
                }
            }).then((body: Collection) => {
                const newCollections = JSON.parse(JSON.stringify(collections))
                console.log(body)
                newCollections.Collections.forEach((c: Collection) => {
                    if (c.CollectionGUID === body.CollectionGUID) {
                        c.Title = body.Title
                        props.setCollection!({ ...props.collection!, Title: body.Title })
                    }
                })
                setCollections(newCollections)
                props.setModalVisible(false)
            })
        } else {
            await createCollection({ Title: props.title }, user.At.Token).then(resp => {
                if (resp.ok) {
                    return resp.json()
                }
            }).then((body: Collection) => {
                const newCollections = JSON.parse(JSON.stringify(collections))
                newCollections.Count += 1
                newCollections.Collections.push(body)
                setCollections(newCollections)
                props.setModalVisible(false)
            })
        }
    }

    return (
        <Modal visible={props.modalVisible} animationType="slide" transparent>
            <TouchableOpacity activeOpacity={1} style={styles.modalOverlay} onPress={() => props.setModalVisible(false)} />
            <View style={styles.modalContainer}>
                <Text type='h1'>Редактирование подборки</Text>
                <Input value={props.title} onChangeText={props.setTitle} />
                <Button type={'primary'} size='large' onPress={saveCollection}>Сохранить</Button>
                <Icons icon='Cross' color='contrast' size={24} padding={8} onPress={() => props.setModalVisible(false)} style={{ position: 'absolute', top: 4, right: 4 }} />
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalOverlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
    },
    modalContainer: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: Colors.Base0,
        width: '100%',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        padding: 16,
        gap: 24,
    },
})
