import React from 'react'
import { StyleSheet } from 'react-native'
import { OtpInput } from "react-native-otp-entry"

import Colors from '../../constants/Colors'


type OTPProps = {
    onFilled: (code: string) => void
    style?: any
}

export default (props: OTPProps) => {
    return (
        <OtpInput
            onFilled={props.onFilled}
            autoFocus
            theme={{
                containerStyle: { ...styles.container, ...props.style },
                inputsContainerStyle: styles.inputsContainer,
                pinCodeContainerStyle: styles.pinCodeContainer,
                pinCodeTextStyle: styles.pinCodeText,
                focusStickStyle: styles.focusStick,
                focusedPinCodeContainerStyle: styles.activePinCodeContainer
            }}
            focusColor={Colors.Orange800}
            focusStickBlinkingDuration={500}
            numberOfDigits={6} />
    )
}

const styles = StyleSheet.create({
    container: {
    },
    inputsContainer: {

    },
    pinCodeContainer: {
        borderWidth: 0,
        borderBottomWidth: 1,
        borderBottomColor: Colors.Base400,
        borderRadius: 0,
    },
    pinCodeText: {
        color: Colors.Base900,
        fontFamily: 'Manrope400',
    },
    focusStick: {
        display: 'none'
    },
    activePinCodeContainer: {
        borderBottomColor: Colors.Orange800,
    },

})
