import React from 'react'
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import Text from './Text'
import Stars from './Stars'
import Icons from './Icons/Icons'
import HeaderChips from './Eatery/HeaderChips'
import { useCollectionsContext } from '../context/collectionsContext'
import { useUserContext } from '../context/userContext'


export type EateryCardProps = {
    AvgBill: number
    AvgRating: number
    Cuisines: string[]
    CollectionGUIDs: string[]
    EstablishmentGUID: string
    EstablishmentTitle: string
    Image: string
    ReviewCount: number
    Type: string
}

export default (props: EateryCardProps & { page: 'collection' | 'search' }) => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()
    const { user } = useUserContext()
    const { collections } = useCollectionsContext()

    const inCollection = (() => {
        if (props.CollectionGUIDs) {
            let res = null
            collections.Collections.forEach(col => {
                props.CollectionGUIDs.forEach(id => {
                    if (col.CollectionGUID === id) {
                        res = col.CollectionGUID
                    }
                })
            })
            return res
        } else {
            return null
        }
    })()

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => navigation.push('Eatery', { id: props.EstablishmentGUID })} style={{ paddingHorizontal: 16 }}>
                <Image source={{ uri: props.Image.length > 0 ? props.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/preview.jpg' }} style={styles.preview} />
            </TouchableOpacity>
            <View style={styles.info}>
                <View style={{ gap: 8 }}>
                    <TouchableOpacity onPress={() => navigation.push('Eatery', { id: props.EstablishmentGUID })}><Text type='h3'>{props.EstablishmentTitle}</Text></TouchableOpacity>
                    <View style={styles.rating}>
                        {
                            props.AvgRating == 0 ?
                                <TouchableOpacity onPress={() => navigation.push('EateryReviews', { id: props.EstablishmentGUID })}>
                                    <Text state='active'>Оцените первым</Text>
                                </TouchableOpacity>
                                :
                                <>
                                    <Stars rating={props.AvgRating} size='small' />
                                    <View style={{ flexDirection: 'row' }}>

                                        <Text>{props.AvgRating.toFixed(1)}</Text>
                                        <Text state='secondary'>/{props.ReviewCount}</Text>
                                    </View>
                                </>
                        }
                    </View>
                </View>
                {
                    !user.Empty && props.page === 'search' ?
                        inCollection !== null ?
                            <Icons icon={'HeartFilled'} color={'default'} size={28} padding={6} onPress={() => navigation.push('Collection', { id: inCollection })} />
                            :
                            <Icons icon={'HeartOutline'} color={'default'} size={28} padding={6} onPress={() => navigation.push('SaveToCollection', { id: props.EstablishmentGUID })} />
                        :
                        <></>
                }
            </View>
            <HeaderChips type={props.Type} cuisines={props.Cuisines} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        gap: 12,
        paddingBottom: 24,
    },
    preview: {
        height: 200,
        width: '100%',
        borderRadius: 8,
    },
    info: {
        paddingHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    rating: {
        flexDirection: 'row',
        alignItems: 'center',
        gap: 4,
    }
})
