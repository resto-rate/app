import React from 'react'
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import Text from './Text'
import Stars from './Stars'


export type EateryCardSmallProps = {
    AvgRating: number
    EstablishmentGUID: string
    EstablishmentTitle: string
    Image: string
}

export default (props: EateryCardSmallProps) => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => navigation.push('Eatery', { id: props.EstablishmentGUID })}>
                <Image source={{ uri: props.Image.length > 0 ? props.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/preview.jpg' }} style={styles.preview} />
            </TouchableOpacity>
            <View style={styles.info}>
                <TouchableOpacity onPress={() => navigation.push('Eatery', { id: props.EstablishmentGUID })}><Text type='h3'>{props.EstablishmentTitle}</Text></TouchableOpacity>
                <View style={styles.ratingContainer}>
                    {
                        props.AvgRating == 0 ?
                            <TouchableOpacity onPress={() => navigation.push('EateryReviews', { id: props.EstablishmentGUID })}>
                                <Text state='active'>Оцените первым</Text>
                            </TouchableOpacity>
                            :
                            <>
                                <Stars rating={props.AvgRating} size='small' />
                                <Text>{props.AvgRating.toFixed(1)}</Text>
                            </>
                    }
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexBasis: '45%',
        gap: 8,
    },
    preview: {
        height: 120,
        width: '100%',
        borderRadius: 8,
    },
    info: {
        gap: 4,
    },
    ratingContainer: {
        flexDirection: 'row',
        gap: 4,
    },
})
