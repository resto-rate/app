import React from 'react'
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { useNavigation } from '@react-navigation/native'

import { prettyDate } from '../utils/Functions'
import { EateryEvent } from '../pages/Eatery/Eatery'

import Text from './Text'


export default (props: EateryEvent) => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    return (
        <TouchableOpacity style={styles.container} onPress={() => AsyncStorage.setItem('event', JSON.stringify(props)).then(() => navigation.push('Event'))}>
            <Image style={styles.preview} source={{ uri: props.Image.length > 0 ? props.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/event_placeholder.jpg' }} />
            <View style={styles.info}>
                <Text>{prettyDate(props.DateTime)}</Text>
                <Text type='h3'>{props.Title}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 260,
        gap: 8,
    },
    preview: {
        borderRadius: 8,
        width: '100%',
        aspectRatio: 3 / 4,
    },
    info: {
        gap: 2,
    },
})
