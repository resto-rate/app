import React from 'react'
import { View, StyleSheet } from 'react-native'

import Text from './Text'

import Star from './Icons/Star.svg'

import Colors from '../constants/Colors'


type RatingProps = {
    rate: number
    text: 'Обслуживание' | 'Питание' | 'Атмосфера' | 'Цена - качество' | 'Время ожидания'
    size: 'medium' | 'large'
    style?: object
}

export default (props: RatingProps) => {
    const starSize = props.size === 'medium' ? 14 : 24

    return (
        <View style={[styles.container, props.style]}>
            <View style={styles.stars}>
                {[...Array(Number(props.rate.toFixed(0)))].map((value: undefined, index: number) =>
                    <Star width={starSize} height={starSize} fill={Colors.Star} key={index} />)}
            </View>
            <Text type={props.size == 'medium' ? 'sec1' : 'default'}>{props.text}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 8,
        paddingHorizontal: 8,
        paddingVertical: 4,
        backgroundColor: Colors.Orange0,
        alignSelf: 'flex-start',
    },
    stars: {
        flexDirection: 'row',
        gap: 2,
    },
})
