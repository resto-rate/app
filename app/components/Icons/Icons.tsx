import React from 'react'
import { TouchableOpacity, View } from 'react-native'

import AddPhoto from './AddPhoto.svg'
import CameraPlus from './CameraPlus.svg'
import ChevronDown from './ChevronDown.svg'
import ChevronLeft from './ChevronLeft.svg'
import ChevronRight from './ChevronRight.svg'
import ChevronUp from './ChevronUp.svg'
import Cross from './Cross.svg'
import Dots from './Dots.svg'
import Edit from './Edit.svg'
import Filter from './Filter.svg'
import Gear from './Gear.svg'
import Geo from './Geo.svg'
import HeartFilled from './HeartFilled.svg'
import HeartOutline from './HeartOutline.svg'
import Network from './Network.svg'
import PaperClip from './PaperClip.svg'
import Phone from './Phone.svg'
import Plus from './Plus.svg'
import RadioBlank from './RadioBlank.svg'
import RadioSelected from './RadioSelected.svg'
import Search from './Search.svg'
import Share from './Share.svg'
import SortDown from './SortDown.svg'
import SortUp from './SortUp.svg'
import Star from './Star.svg'
import StarEmpty from './StarEmpty.svg'
import StarHalf from './StarHalf.svg'
import Time from './Time.svg'
import Trash from './Trash.svg'
import User from './User.svg'
import Colors from '../../constants/Colors'
import { capitalizeFirst } from '../../utils/Functions'

type IconsProps = {
    icon: 'AddPhoto' | 'CameraPlus' | 'ChevronDown' | 'ChevronLeft' | 'ChevronRight' | 'ChevronUp' | 'Cross' | 'Dots' | 'Edit' | 'Filter' | 'Gear' | 'Geo' | 'HeartFilled' | 'HeartOutline' | 'Network' | 'PaperClip' | 'Phone' | 'Plus' | 'RadioBlank' | 'RadioSelected' | 'Search' | 'Share' | 'SortDown' | 'SortUp' | 'Star' | 'StarEmpty' | 'StarHalf' | 'Time' | 'Trash' | 'User'
    color: 'default' | 'active' | 'contrast',
    size: number,
    padding: number,
    onPress?: () => void
    style?: any
}

export default (props: IconsProps) => {
    const icons = {
        AddPhoto: <AddPhoto fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        CameraPlus: <CameraPlus fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        ChevronDown: <ChevronDown stroke={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        ChevronLeft: <ChevronLeft fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        ChevronRight: <ChevronRight stroke={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        ChevronUp: <ChevronUp fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Cross: <Cross fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Dots: <Dots stroke={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Edit: <Edit fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Filter: <Filter fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Gear: <Gear fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Geo: <Geo fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        HeartFilled: <HeartFilled fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        HeartOutline: <HeartOutline stroke={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Network: <Network fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        PaperClip: <PaperClip fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Phone: <Phone fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Plus: <Plus stroke={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        RadioBlank: <RadioBlank fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        RadioSelected: <RadioSelected fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Search: <Search fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Share: <Share stroke={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        SortDown: <SortDown fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        SortUp: <SortUp fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Star: <Star fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        StarEmpty: <StarEmpty fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        StarHalf: <StarHalf fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Time: <Time fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        Trash: <Trash fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
        User: <User fill={Colors[`Icon${capitalizeFirst(props.color)}`]} width={props.size} height={props.size} />,
    }

    if (props.onPress) {
        return (
            <TouchableOpacity onPress={props.onPress} style={[{ padding: props.padding }, props.style]}>
                {
                    icons[props.icon]
                }
            </TouchableOpacity>
        )
    } else {
        return (
            <View style={[{ padding: props.padding }, props.style]}>
                {
                    icons[props.icon]
                }
            </View>
        )
    }
}
