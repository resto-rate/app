import React from 'react'
import ImageView from 'react-native-image-viewing'

type ImageCarouselProps = {
    images: string[]
    onRequestClose: () => void
    imageIndex: number
    visible: boolean
}

export default (props: ImageCarouselProps) => {
    return <ImageView {...props} images={props.images.map(el => ({ uri: el }))} doubleTapToZoomEnabled={false} />
}
