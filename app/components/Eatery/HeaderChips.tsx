import React from 'react'
import { ScrollView } from 'react-native'

import Chips from '../Chips'


type HeaderChipsProps = {
    type: string
    cuisines: string[]
}

export default (props: HeaderChipsProps) =>
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{ paddingHorizontal: 16, gap: 8, marginTop: 8 }}>
        <Chips color='black'>{props.type}</Chips>
        {
            props.cuisines.map((cuisine, index) => <Chips key={index} color='orange'>{cuisine}</Chips>)
        }
    </ScrollView>
