import React, { useEffect, useState } from 'react'
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native'

import { getEstablishment } from '../../api/establishment'
import { switchEnding } from '../../utils/Functions'

import Text from '../Text'
import Stars from '../Stars'
import ImageCarousel from '../ImageCarousel'
import { Eatery } from '../../pages/Eatery/Eatery'

import Colors from '../../constants/Colors'


type HeaderProps = {
    Establishment: string | Eatery
}

export default (props: HeaderProps) => {
    const [eateryLogoVisible, setEateryLogoVisible] = useState(false)
    const [eatery, setEatery] = useState<Eatery | null>(null)

    const fetchEstablishment = async () => {
        if (typeof props.Establishment === 'object') {
            setEatery(props.Establishment)
            return
        }
        const establishmentId = props.Establishment

        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getEstablishment(establishmentId).then(resp => {
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                setEatery({ ...body })
            } else {
                console.log('Wrong establishment id')
            }
        })
    }

    useEffect(() => {
        fetchEstablishment()
    }, [])

    if (eatery === null)
        return <Text style={{ paddingHorizontal: 16 }}>Загрузка...</Text>

    return (
        <View style={styles.container}>
            <ImageCarousel images={[eatery.EstablishmentLogo]} imageIndex={0} onRequestClose={() => setEateryLogoVisible(false)} visible={eateryLogoVisible} />
            <TouchableOpacity onPress={() => setEateryLogoVisible(true)}>
                <Image style={styles.logo} source={{ uri: eatery.EstablishmentLogo }} />
            </TouchableOpacity>
            <View style={{ gap: 8 }}>
                <Text type='h1'>{eatery.EstablishmentTitle}</Text>
                <View style={styles.rating}>
                    <Stars rating={eatery.Rating.AvgRating} size='small' />
                    <Text type='sec1'>{eatery.Rating.AvgRating.toFixed(1)}</Text>
                    <Text type='sec1' state='secondary'>({eatery.Rating.ReviewCount} {switchEnding(['отзывов', 'отзыв', 'отзыва'], eatery.Rating.ReviewCount)})</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        gap: 8,
        paddingVertical: 8,
        paddingHorizontal: 16,
        backgroundColor: Colors.Orange0,
        alignItems: 'center',
    },
    logo: {
        width: 76,
        height: 76,
        borderRadius: 38,
    },
    rating: {
        flexDirection: 'row',
        alignItems: 'center',
        gap: 4,
    },
    stars: {
        flexDirection: 'row',
    },
})
