import React from 'react'
import { View, StyleSheet } from 'react-native'

import Text from '../Text'

import Icons from '../Icons/Icons'

import Colors from '../../constants/Colors'


type MenuTabsProps = {
    categories: string[]
}

export default (props: MenuTabsProps) => {
    return (
        <View style={styles.container}>
            <Icons icon='ChevronLeft' size={24} padding={8} color='default' onPress={() => 0} />
            {
                props.categories.map(category => (
                    <View key={category}>
                        <Text>{category}</Text>
                    </View>
                ))
            }
            <Icons icon='ChevronRight' size={24} padding={8} color='default' onPress={() => 0} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.Orange0,
        borderTopColor: Colors.Orange100,
        borderTopWidth: 1,
    }
})
