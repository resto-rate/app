import React, { useState } from 'react'
import { View, ScrollView, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import { switchCriterion, switchEnding } from '../../utils/Functions'

import Stars from '../Stars'
import Button from '../Button'
import Text from '../Text'
import ImageCarousel from '../ImageCarousel'
import Icons from '../Icons/Icons'

import Colors from '../../constants/Colors'


export type RatingCardProps = {
    AvgRating: number
    ReviewCount: number
    AvgRates: {
        Service: number
        Food: number
        Vibe: number
        PriceQuality: number
        WaitingTime: number
    }
    Images: string[]
}

export default (props: RatingCardProps) => {
    const route = useRoute()
    const navigation = useNavigation<NativeStackNavigationProp<any>>()
    const [imageView, setImageView] = useState({ imageIndex: 0, visible: false })

    return (
        <View style={styles.container}>
            <ImageCarousel images={props.Images} {...imageView} onRequestClose={() => setImageView({ imageIndex: 0, visible: false })} />
            <Text type='h2' style={{ marginHorizontal: 12 }}>Рейтинг</Text>
            <TouchableOpacity style={styles.ratingCard} onPress={() => navigation.push('EateryReviews', { id: route.params.id })}>
                <Text style={styles.rating}>{props.AvgRating.toFixed(1)}</Text>
                <View style={{ flex: 1 }}>
                    <Stars rating={props.AvgRating} size='medium' />
                    <Text type='sec2'>{props.ReviewCount} {switchEnding(['отзывов', 'отзыв', 'отзыва'], props.ReviewCount)}</Text>
                </View>
                <Icons icon={'ChevronRight'} size={32} color='active' padding={0} />
            </TouchableOpacity>
            <ScrollView contentContainerStyle={{ gap: 8, paddingHorizontal: 12 }} showsHorizontalScrollIndicator={false} horizontal={true}>
                {
                    props.Images.map((el, ind) =>
                        <TouchableOpacity key={ind} onPress={() => setImageView({ imageIndex: ind, visible: true })}>
                            <Image style={styles.photo} source={{ uri: el.toString() }} />
                        </TouchableOpacity>
                    )
                }
            </ScrollView>
            <View style={styles.evaluatesContainer}>
                <Text type='h3'>Оценки по критериям</Text>
                <View style={{ gap: 8 }}>
                    {
                        Object.keys(props.AvgRates).map((criterion, index) =>
                            <View key={index} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text type='sec1'>{switchCriterion(criterion.toLowerCase())}</Text>
                                <Stars rating={props.AvgRates[criterion]} size='small' reverse />
                            </View>
                        )
                    }
                </View>
            </View>
            <Button style={{ marginHorizontal: 12 }} onPress={() => navigation.push('NewReview', { id: route.params.id, eateryTitle: 'Placeholder' })} size='medium' type='primary'>Оставить отзыв</Button>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 24,
        marginHorizontal: 16,
        backgroundColor: Colors.Orange0,
        borderRadius: 8,
        paddingVertical: 12,
        gap: 16,
    },
    ratingCard: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        gap: 12,
        marginHorizontal: 12,
    },
    rating: {
        fontSize: 36,
        fontWeight: '600',
    },
    photo: {
        width: 76,
        height: 76,
        borderRadius: 8,
    },
    evaluatesContainer: {
        marginHorizontal: 12,
        gap: 8,
    },
})
