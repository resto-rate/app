import React from 'react'
import { StyleSheet, View } from 'react-native'

import Star from './Icons/Star.svg'
import StarHalf from './Icons/StarHalf.svg'
import StarEmpty from './Icons/StarEmpty.svg'

import Colors from '../constants/Colors'


type StarsProps = {
    rating: number,
    size: 'medium' | 'small',
    reverse?: boolean,
}

export default (props: StarsProps) => {
    const sz = props.size == 'medium' ? 24 : 20
    return (
        <View style={[styles.container, props.reverse && styles.reverse]}>
            {[0, 0, 0, 0, 0].map((el, i) => {
                if (props.rating < i + 0.25) {
                    return <StarEmpty key={i} width={sz} height={sz} fill={Colors.Star} />
                } else if (i + 0.25 <= props.rating && props.rating < i + 0.75) {
                    return <StarHalf key={i} width={sz} height={sz} fill={Colors.Star} style={props.reverse ? { transform: [{ rotate: '180deg' }] } : {}} />
                } else {
                    return <Star key={i} width={sz} height={sz} fill={Colors.Star} />
                }
            })}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        gap: 2,
    },
    reverse: {
        flexDirection: 'row-reverse',
    },
})
