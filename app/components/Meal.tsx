import React, { useEffect, useState } from 'react'
import { View, Image, StyleSheet, TouchableOpacity, Modal, StatusBar, Platform } from 'react-native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { useNavigation } from '@react-navigation/native'

import Text from './Text'
import Chips from './Chips'
import Icons from './Icons/Icons'

import Colors from '../constants/Colors'
import { switchEnding } from '../utils/Functions'


export type Meal = {
    Calories: number
    Carbohydrates: number
    Category: string
    Composition: string
    DishGUID: string
    DishImage: string
    DishName: string
    EstablishmentGUID: string
    Fats: number
    LikeCount: number
    Price: number
    Proteins: number
    Volume: number
    Weight: number
}

type MealProps = {
    meal: Meal
    style?: any
}

export default (props: MealProps) => {
    const [modalVisible, setModalVisible] = useState(false)
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    useEffect(() => {
        if (modalVisible) {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.ModalOverlay)
            navigation.setOptions({ navigationBarColor: Colors.Base0 })
        } else {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.Background)
            navigation.setOptions({ navigationBarColor: Colors.Background })
        }
    }, [modalVisible])

    return (
        <>
            <TouchableOpacity style={[styles.container, props.style]} onPress={() => setModalVisible(true)}>
                <Image style={styles.preview} source={{ uri: props.meal.DishImage.length > 0 ? props.meal.DishImage : 'https://restorate.hb.ru-msk.vkcs.cloud/static/preview.jpg' }} />
                <Text type='sec1' numberOfLines={1}>{props.meal.DishName}</Text>
                <View style={styles.info}>
                    <Text>{props.meal.Price} ₽</Text>
                    <Text type='sec2'>{props.meal.Weight == 0 ? props.meal.Volume + ' мл' : props.meal.Weight + ' г'}</Text>
                </View>
            </TouchableOpacity>
            <Modal visible={modalVisible} animationType="slide" transparent>
                <TouchableOpacity activeOpacity={1} style={styles.modalOverlay} onPress={() => setModalVisible(false)} />
                <View style={styles.modalContainer}>
                    <Image style={styles.modalPhoto} source={{ uri: props.meal.DishImage.length > 0 ? props.meal.DishImage : 'https://restorate.hb.ru-msk.vkcs.cloud/static/preview.jpg' }} />
                    <View style={styles.modalInfo}>
                        <Chips color='black'>{props.meal.LikeCount > 0 ?  `Понравилось ${props.meal.LikeCount} ${switchEnding(['посетителям', 'посетителю', 'посетителям'], props.meal.LikeCount)}` : 'Нет оценок'}</Chips>
                        <Text type='h1'>{props.meal.DishName}</Text>
                        {
                            props.meal.Composition ?
                                <Text type='sec1' state='secondary'>Состав: {props.meal.Composition}</Text>
                                : <></>
                        }
                        {
                            props.meal.Calories ?
                                <View style={styles.info}>
                                    <Text type='sec1' state='secondary'>К/Б/Ж/У</Text>
                                    <Text type='sec1' state='secondary'>{props.meal.Calories}/{props.meal.Proteins}/{props.meal.Fats}/{props.meal.Carbohydrates}</Text>
                                </View>
                                : <></>
                        }
                        <View style={styles.info}>
                            <Text type='h2'>{props.meal.Price} ₽</Text>
                            <Text type='h2'>{props.meal.Weight == 0 ? props.meal.Volume + ' мл' : props.meal.Weight + ' г'}</Text>
                        </View>
                    </View>
                    <Icons icon='Cross' color='contrast' size={24} padding={8} onPress={() => setModalVisible(false)} style={{ position: 'absolute', top: 4, right: 4 }} />
                </View>
            </Modal>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexBasis: '45%',
        gap: 4,
    },
    preview: {
        height: 124,
        width: '100%',
        borderRadius: 4,
    },
    info: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },
    modalOverlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
    },
    modalContainer: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: Colors.Base0,
        width: '100%',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
    },
    modalPhoto: {
        height: 232,
        width: '100%',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
    },
    modalInfo: {
        padding: 16,
        gap: 8,
        paddingBottom: 32,
    },
})
