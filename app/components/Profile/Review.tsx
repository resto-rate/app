import React, { useState } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { prettyDate, switchCriterion } from '../../utils/Functions'

import { useUserContext } from '../../context/userContext'
import { deleteReview } from '../../api/establishment'

import Rating from '../Rating'
import Text from '../Text'
import ImageCarousel from '../ImageCarousel'
import Icons from '../Icons/Icons'
import Colors from '../../constants/Colors'


export type Review = {
    ReviewGUID: string
    Establishment: {
        EstablishmentGUID: string
        EstablishmentImage: string
        EstablishmentTitle: string
    },
    Rating: {
        Service: number
        Food: number
        Vibe: number
        WaitingTime: number
        PriceQuality: number
    },
    Comment: string
    LikedTheMost: string
    NeedToBeChanged: string
    Images: string[]
    Timestamp: number
    User: {
        AccountGUID: string
        Name: string
        Surname: string
        UserImage: string
    }
}

type ReviewProps = {
    review: Review
    page: 'profile' | 'eatery'
    onTrashPressed?: () => void
}

export default (props: ReviewProps) => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()
    const [imageView, setImageView] = useState({ imageIndex: 0, visible: false })

    const { user } = useUserContext()

    const navigate = () => {
        if (props.page !== 'profile') {
            navigation.push('User', { id: props.review.User.AccountGUID })
        } else {
            navigation.push('Eatery', { id: props.review.Establishment.EstablishmentGUID })
        }
    }

    const del = async () => {
        await deleteReview(props.review.ReviewGUID, user.At.Token).then(resp => {
            if (resp.ok && props.onTrashPressed) {
                props.onTrashPressed()
            }
        })
    }

    return (
        <View style={styles.container}>
            <ImageCarousel images={props.review.Images} {...imageView} onRequestClose={() => setImageView({ imageIndex: 0, visible: false })} />
            <View style={styles.place}>
                <TouchableOpacity onPress={navigate} style={styles.placeInfo}>
                    {
                        props.page !== 'profile' ?
                            props.review.User.UserImage.length > 0 ?
                                <Image style={styles.placeLogo} source={{ uri: props.review.User.UserImage }} />
                                :
                                <View style={styles.placeLogo}>
                                    <Text type='h2' state='contrast'>{props.review.User.Name.substring(0, 1)} {props.review.User.Surname.substring(0, 1)}</Text>
                                </View>
                            :
                            <Image style={styles.placeLogo} source={{ uri: props.review.Establishment.EstablishmentImage }} />
                    }
                    <View style={{ gap: 2 }}>
                        <Text type='h3'>{props.page !== 'profile' ? `${props.review.User.Name}${props.review.User.Surname}` : props.review.Establishment.EstablishmentTitle}</Text>
                        <Text type='sec2' state='secondary'>{prettyDate(props.review.Timestamp * 1000)}</Text>
                    </View>
                </TouchableOpacity>
                {
                    props.page === 'profile' && props.onTrashPressed ?
                        <Icons icon={'Trash'} color={'default'} size={24} padding={8} onPress={del} />
                        :
                        <></>
                }
            </View>
            <ScrollView contentContainerStyle={styles.rates} showsHorizontalScrollIndicator={false} horizontal={true}>
                {
                    Object.keys(props.review.Rating).map((criterion, index) =>
                        <Rating key={index} rate={props.review.Rating[criterion]} text={switchCriterion(criterion.toLowerCase())} size='medium' />
                    )
                }
            </ScrollView>
            <Text style={styles.text} type='sec1'><Text type='sec1' style={{ fontWeight: '700' }}>Понравилось: </Text>{props.review.LikedTheMost}</Text>
            <Text style={styles.text} type='sec1'><Text type='sec1' style={{ fontWeight: '700' }}>Не понравилось: </Text>{props.review.NeedToBeChanged}</Text>
            <Text style={styles.text} type='sec1'><Text type='sec1' style={{ fontWeight: '700' }}>Комментарий: </Text>{props.review.Comment}</Text>
            <ScrollView contentContainerStyle={styles.photos} showsHorizontalScrollIndicator={false} horizontal={true}>
                {
                    props.review.Images.map((el, ind) =>
                        <TouchableOpacity key={ind} onPress={() => setImageView({ imageIndex: ind, visible: true })}>
                            <Image style={styles.photo} source={{ uri: el.toString() }} />
                        </TouchableOpacity>
                    )
                }
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        gap: 12,
    },
    place: {
        paddingHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    placeInfo: {
        flexDirection: 'row',
        gap: 8,
    },
    placeLogo: {
        backgroundColor: Colors.Orange800,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: 48,
        height: 48,
        borderRadius: 24
    },
    rates: {
        gap: 8,
        paddingHorizontal: 16,
        paddingVertical: 4,
    },
    text: {
        paddingHorizontal: 16,
    },
    photos: {
        paddingHorizontal: 16,
        gap: 8,
        minWidth: Dimensions.get('screen').width,
    },
    photo: {
        width: 76,
        height: 76,
        borderRadius: 8,
    },
})
