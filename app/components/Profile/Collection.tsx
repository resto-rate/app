import React from 'react'
import { StyleSheet, Image, TouchableOpacity } from 'react-native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { useNavigation } from '@react-navigation/native'

import { switchEnding } from '../../utils/Functions'
import { Collection } from '../../context/collectionsContext'

import Text from '../Text'


export default (props: Collection & { onPress?: (CollectionGUID: string) => void, style?: any }) => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    return (
        <TouchableOpacity onPress={() => props.onPress ? props.onPress(props.CollectionGUID) : navigation.push('Collection', { id: props.CollectionGUID })} style={[styles.container, props.style]}>
            <Image style={styles.preview} source={{ uri: props.Preview.length > 0 ? props.Preview : 'https://restorate.hb.ru-msk.vkcs.cloud/static/preview.jpg' }} />
            <Text type='h3' numberOfLines={1}>{props.Title}</Text>
            <Text type='sec2' state='secondary'>{props.EstablishmentCount} {switchEnding(['мест', 'место', 'места'], props.EstablishmentCount)}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        gap: 4,
        flex: 1,
        flexBasis: '45%'
    },
    preview: {
        width: '100%',
        height: 130,
        borderRadius: 12,
        marginBottom: 4,
    },
})
