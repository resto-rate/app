import React from 'react'
import { TextInput, StyleSheet } from 'react-native'

import Colors from '../constants/Colors'


type InputProps = {
    value: string
    onChangeText: (text: string) => void
    placeholder?: string
    autoFocus?: boolean
    multiline?: boolean
    style?: any
}

export default (props: InputProps) =>
    <TextInput
        style={[styles.input, props.style]}
        value={props.value}
        onChangeText={props.onChangeText}
        placeholder={props.placeholder}
        autoFocus={props.autoFocus}
        placeholderTextColor={Colors.Orange300}
        multiline={props.multiline} />

const styles = StyleSheet.create({
    input: {
        paddingVertical: 16,
        paddingHorizontal: 12,
        backgroundColor: Colors.Orange50,
        color: Colors.Orange800,
        borderRadius: 8,
        fontFamily: 'Manrope400',
        fontSize: 16,
        verticalAlign: 'top',
    },
})
