import React from 'react'
import { Text, ActivityIndicator, StyleSheet, TouchableOpacity } from 'react-native'

import Colors from '../constants/Colors'


type ButtonProps = {
    children: any
    onPress: () => void
    type: 'primary' | 'secondary' | 'text'
    size: 'small' | 'medium' | 'large'
    disabled?: boolean
    loading?: boolean
    style?: object
}

export default (props: ButtonProps) => {
    return (
        <TouchableOpacity disabled={props.disabled} onPress={props.onPress} style={[styles.button, styles[props.size + 'Button'], styles[props.type + 'Button'], props.style, props.disabled && styles[props.type + 'Disabled']]}>
            <Text style={[styles.label, styles[props.size + 'Label'], styles[props.type + 'Label'], props.disabled && styles[props.type + 'Disabled'], props.loading && styles.loaderText]}>
                {props.children}
            </Text>
            {
                props.loading &&
                <ActivityIndicator style={styles.loader} color={props.type == 'primary' ? Colors.Base0 : Colors.Orange800} />
            }
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 8,
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center',
    },
    label: {
        textAlign: 'center',
        fontWeight: '500',
    },
    primaryButton: {
        backgroundColor: Colors.Orange800,
    },
    secondaryButton: {
        backgroundColor: Colors.Orange50,
    },
    textButton: {
        backgroundColor: 'transparent',
        paddingVertical: 8,
    },
    primaryLabel: {
        color: Colors.Base0,
    },
    secondaryLabel: {
        color: Colors.Orange800,
    },
    textLabel: {
        color: Colors.Orange800,
    },
    smallButton: {
        paddingVertical: 8,
    },
    mediumButton: {
        paddingVertical: 10,
    },
    largeButton: {
        paddingVertical: 12,
    },
    smallLabel: {
        fontSize: 14,
    },
    mediumLabel: {
        fontSize: 16,
    },
    largeLabel: {
        fontSize: 18,
    },
    primaryDisabled: {
        backgroundColor: Colors.Base400,
        color: Colors.Base0,
    },
    secondaryDisabled: {
        backgroundColor: Colors.Base400,
        color: Colors.Base0,
    },
    textDisabled: {
        color: Colors.Base400,
        backgroundColor: 'transparent',
    },
    loaderText: {
        color: 'transparent',
    },
    loader: {
        position: 'absolute',
    }
})
