import React, { ReactNode, useEffect, useState } from 'react'
import { View, TouchableOpacity, Modal, StyleSheet, StatusBar, Platform } from 'react-native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { useNavigation } from '@react-navigation/native'

import Text from '../Text'
import Button from '../Button'
import Icons from '../Icons/Icons'

import Colors from '../../constants/Colors'


type OptionType = {
    children: any
    text: string
    icon: ReactNode
    modalTitle: string
    modalOnSave: () => Promise<boolean>
    modalSaving: boolean
    saveDisabled?: boolean
    style?: object
}

export default (props: OptionType) => {
    const [modalVisible, setModalVisible] = useState(false)
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    const save = async () => {
        const res = await props.modalOnSave()
        if (res) {
            setModalVisible(false)
        }
    }


    useEffect(() => {
        if (modalVisible) {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.ModalOverlay)
            navigation.setOptions({ navigationBarColor: Colors.Base0 })
        } else {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.Background)
            navigation.setOptions({ navigationBarColor: Colors.Background })
        }
    }, [modalVisible])

    return (
        <>
            <TouchableOpacity onPress={() => setModalVisible(true)} style={styles.container}>
                <View style={styles.title}>
                    {props.icon}
                    <Text>{props.text}</Text>
                </View>
                <Icons icon={'Edit'} color={'default'} size={24} padding={0} />
            </TouchableOpacity >
            <Modal visible={modalVisible} animationType="slide" transparent>
                <TouchableOpacity activeOpacity={1} style={styles.modalOverlay} onPress={() => setModalVisible(false)} />
                <View style={styles.modalContainer}>
                    <Text type='h3'>{props.modalTitle}</Text>
                    <View style={[{ paddingVertical: 28 }, props.style]}>
                        {props.children}
                    </View>
                    <Button type='primary' size='large' onPress={save} loading={props.modalSaving} disabled={props.saveDisabled}>Сохранить</Button>
                    <TouchableOpacity style={styles.cross} onPress={() => setModalVisible(false)}>
                        <Icons icon={'Cross'} color={'default'} size={24} padding={0} />
                    </TouchableOpacity>
                </View>
            </Modal>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 16,
        borderBottomWidth: 1,
        borderBottomColor: Colors.Divider,
    },
    title: {
        flexDirection: 'row',
        gap: 8,
        alignItems: 'center',
    },
    modalOverlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
    },
    modalContainer: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: Colors.Base0,
        width: '100%',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        paddingHorizontal: 16,
        paddingTop: 24,
        paddingBottom: 48,
    },
    cross: {
        position: 'absolute',
        top: 24,
        paddingBottom: 8,
        paddingLeft: 8,
        right: 16,
    },
})
