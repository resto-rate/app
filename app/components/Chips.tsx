import React from 'react'
import { View, StyleSheet } from 'react-native'

import Text from './Text'

import Colors from '../constants/Colors'


type ChipsProps = {
    children: any
    color: 'black' | 'orange'
}

export default (props: ChipsProps) =>
    <View style={[styles.chip, styles[props.color]]}>
        <Text style={styles[props.color]} type='sec1'>{props.children}</Text>
    </View>

const styles = StyleSheet.create({
    chip: {
        paddingHorizontal: 8,
        paddingVertical: 4,
        borderRadius: 100,
        alignSelf: 'flex-start',
    },
    black: {
        color: Colors.Base50,
        backgroundColor: Colors.Base900,
    },
    orange: {
        color: Colors.Orange800,
        backgroundColor: Colors.Orange50,
    },
})
