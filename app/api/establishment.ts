import { API_URL } from "../constants/Config"
import { NewReview } from "../pages/Eatery/NewReview";
import { encodeURISearchParams } from "../utils/Functions";


export const getEstablishment = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}`, {
        method: 'GET'
    });
}

export const getEstablishmentMenu = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/menu`, {
        method: 'GET'
    })
}

export const getEstablishmentPromotions = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/promotions`, {
        method: 'GET'
    })
}

export const getEstablishmentEvents = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/events`, {
        method: 'GET'
    })
}

export const searchEstablishments = async (page: number, params?: object) => {
    return fetch(API_URL + `/establishments/search?page=${page}${params ? encodeURISearchParams(params) : ""}`, {
        method: 'GET'
    })
}

export const getInputVars = async () => {
    return fetch(API_URL + `/establishments/inputvars`, {
        method: 'GET'
    })
}

export const searchAddress = async (address: string) => {
    return fetch(API_URL + `/establishments/inputaddress?fulltextfind=${address}`, {
        method: 'GET'
    })
}

export const getUserReviews = async (AccountGUID: string) => {
    return fetch(API_URL + `/establishments/reviewers/${AccountGUID}/reviews`, {
        method: 'GET'
    })
}

export const getEstablishmentReviews = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/reviews`, {
        method: 'GET'
    })
}

export const createReview = async (body: NewReview, EstablishmentGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/review/create`, {
        method: 'POST',
        headers: {
            Authorization: AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const deleteReview = async (ReviewGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/reviews/${ReviewGUID}`, {
        method: 'DELETE',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}

export const uploadImage = async (formData: FormData, AuthorizationToken: string) => {
    return fetch(API_URL + '/establishments/uploadImage', {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: formData
    })
}

export const getCollections = async (AccountGUID: string) => {
    return fetch(API_URL + `/establishments/users/${AccountGUID}/collections`, {
        method: 'GET'
    })
}

export const getCollection = async (CollectionGUID: string) => {
    return fetch(API_URL + `/establishments/collections/${CollectionGUID}`, {
        method: 'GET'
    })
}

export const createCollection = async (body: { Title: string, CollectionGUID?: string }, AuthorizationToken: string) => {
    return fetch(API_URL + '/establishments/collections/create', {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const addToCollection = async (body: { EstablishmentGUID: string }, CollectionGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/collections/${CollectionGUID}/add`, {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const removeFromCollection = async (CollectionGUID: string, EstablishmentGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/collections/${CollectionGUID}/delete/${EstablishmentGUID}`, {
        method: 'DELETE',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}
