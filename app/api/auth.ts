import { API_URL } from "../constants/Config"

export const getOtpCode = async (body: { Login: string }) => {
    return fetch(API_URL + '/auth/otp', {
        method: 'POST',
        body: JSON.stringify(body)
    })
}

export const loginOtp = async (body: { Login: string, OTP: string, OtpGUID: string }) => {
    return fetch(API_URL + '/auth/loginotp', {
        method: 'POST',
        body: JSON.stringify(body)
    })
}

export const getProfile = async (AuthorizationToken: string) => {
    return fetch(API_URL + '/auth/profile/data', {
        method: 'GET',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}

export const changeProfileData = async (body: { Name?: string, Surname?: string, City?: string }, AuthorizationToken: string) => {
    return fetch(API_URL + '/auth/profile/changeProfileData', {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const logout = async (AuthorizationToken: string) => {
    return fetch(API_URL + '/auth/profile/logout', {
        method: 'DELETE',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}

export const changeProfileImage = async (formData: FormData, AuthorizationToken: string) => {
    return fetch(API_URL + '/auth/profile/changeImage', {
        method: 'POST',
        body: formData,
        headers: {
            'Authorization': AuthorizationToken
        },
    })
}

export const notificationRegister = async (body: { AccountGUID: string | null, Token: string }) => {
    return fetch(API_URL + '/notification/register', {
        method: 'POST',
        body: JSON.stringify(body)
    })
}

export const getCities = async () => {
    return fetch(API_URL + '/auth/cities', {
        method: 'GET'
    })
}
