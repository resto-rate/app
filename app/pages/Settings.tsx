import React, { useEffect, useState } from 'react'
import { Platform, StyleSheet, TouchableOpacity, View, ScrollView, Dimensions, Image, Modal, StatusBar } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { launchImageLibrary, launchCamera, Asset } from 'react-native-image-picker'

import { displayName } from '../../app.json'
import { version } from '../../package.json'
import { capitalizeFirst, shareText } from '../utils/Functions'
import { changeProfileData, changeProfileImage, getCities, logout as logoutAPI } from '../api/auth'
import { initialUserContextState, useUserContext } from '../context/userContext'

import Text from '../components/Text'
import Input from '../components/Input'
import Button from '../components/Button'
import Option from '../components/Settings/Option'

import Icons from '../components/Icons/Icons'

import Colors from '../constants/Colors'


export default () => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Настройки',
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerTitle: ({ children }) => <Text type='h3'>{children}</Text>,
            headerLeft: () => <Icons icon='ChevronLeft' color='default' size={24} padding={8} onPress={() => navigation.goBack()} />,
        });
    }, [navigation]);


    const { user, setUser } = useUserContext()
    const [name, setName] = useState(user.Name)
    const [surname, setSurname] = useState(user.Surname)
    const [nameSaving, setNameSaving] = useState(false)

    const [cities, setCities] = useState<string[]>([])
    const [newCity, setNewCity] = useState(user.City)
    const [citySaving, setCitySaving] = useState(false)

    const [newPhoto, setNewPhoto] = useState<Asset | undefined>(undefined)
    const [photoSaving, setPhotoSaving] = useState(false)

    const [feedback, setFeedback] = useState('')
    const [isFeedbackOpened, setIsFeedbackOpened] = useState(false)

    const fetchCities = async () => {
        await getCities().then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                setCities(body.Cities.sort())
            }
        })
    }

    useEffect(() => {
        fetchCities()
    }, [])

    const sendNewUserData = async () => {
        setNameSaving(true)
        if (!name || !surname) {
            console.log('Имя и фамилия не могут быть пустыми!')
            setNameSaving(false)
            return false
        }
        return changeProfileData({ Name: capitalizeFirst(name.replace(/\s/g, "")), Surname: capitalizeFirst(surname.replace(/\s/g, "")) }, user.At.Token).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
            setNameSaving(false)
            return resp.json()
        }).then(body => {
            if (body) {
                setUser({ ...user, ...body, Empty: false })
                setNameSaving(false)
                return true
            }
            return false
        }).catch(err => {
            setNameSaving(false)
            console.log(err)
            return false
        })
    }

    const updateLocation = async () => {
        AsyncStorage.setItem('city', newCity)
        setCitySaving(true)
        if (user.Empty) {
            setUser({ ...user, City: newCity })
            setCitySaving(false)
            return true
        }
        return changeProfileData({ City: newCity }, user.At.Token).then(resp => {
            if (resp.ok) {
                setUser({ ...user, City: newCity })
                setCitySaving(false)
                return true
            }
            setCitySaving(false)
            return false
        })
    }

    const logout = () => {
        setUser(initialUserContextState.user)
        AsyncStorage.removeItem('user')
        logoutAPI(user.At.Token)
        navigation.goBack()
    }

    const handleChoosePhoto = async () => {
        const result = await launchImageLibrary({ mediaType: 'photo', maxWidth: 512, maxHeight: 512 });
        if (!result.errorCode && !result.didCancel) {
            setNewPhoto(result.assets![0])
        } else {
            console.log(result)
        }
    }

    const handleMakePhoto = async () => {
        const result = await launchCamera({ mediaType: 'photo', saveToPhotos: false, maxWidth: 512, maxHeight: 512 });
        if (!result.errorCode && !result.didCancel) {
            setNewPhoto(result.assets![0])
        } else {
            console.log(result)
        }
    }

    const sendNewPhoto = async () => {
        setPhotoSaving(true)
        let formData = new FormData()
        if (newPhoto) {
            formData.append("imageFile", { ...newPhoto, name: newPhoto.fileName })
            try {
                const result = await changeProfileImage(formData, user.At.Token)
                const data = await result.json()
                setUser({ ...user, ...data })
                setPhotoSaving(false)
                setUser({ ...user, ImageURL: data.ImageURL })
                return true
            } catch (error) {
                console.error(error)
                setPhotoSaving(false)
                return false
            }
        }
        return false
    }

    useEffect(() => {
        if (isFeedbackOpened) {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.ModalOverlay)
            navigation.setOptions({ navigationBarColor: Colors.Base0 })
        } else {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.Background)
            navigation.setOptions({ navigationBarColor: Colors.Background })
        }
        setFeedback('')
    }, [isFeedbackOpened])

    return (
        <View style={styles.container}>
            <Option icon={<Icons icon='Geo' color='default' size={24} padding={0} />} text={user.City} modalTitle={'Изменение региона поиска'} modalSaving={citySaving} modalOnSave={updateLocation}>
                <ScrollView style={styles.citySelectContainer}>
                    {
                        cities.map((city, index) =>
                            <TouchableOpacity key={index} style={styles.citySelectItem} onPress={() => setNewCity(city)}>
                                <Text>{city}</Text>
                                <Icons icon={newCity === city ? 'RadioSelected' : 'RadioBlank'} color={newCity === city ? 'active' : 'default'} size={24} padding={0} />
                            </TouchableOpacity>
                        )
                    }
                </ScrollView>
            </Option>
            {
                !user.Empty &&
                <>
                    <Option icon={<Icons icon='User' color='default' size={24} padding={0} />} text={user.Name + ' ' + user.Surname} modalTitle={'Изменение личных данных'} modalOnSave={sendNewUserData} modalSaving={nameSaving} saveDisabled={!name.length || !surname.length}>
                        <Input value={name} onChangeText={setName} placeholder='Имя' style={{ marginBottom: 20 }} autoFocus />
                        <Input value={surname} onChangeText={setSurname} placeholder='Фамилия' />
                    </Option>
                    <Option icon={<Icons icon='CameraPlus' color='default' size={24} padding={0} />} text={'Изменить фото профиля'} modalTitle={'Изменение фото'} modalOnSave={sendNewPhoto} modalSaving={photoSaving} style={{ gap: 8 }} saveDisabled={!newPhoto}>
                        <Image source={{ uri: newPhoto ? newPhoto.uri : 'https://restorate.hb.ru-msk.vkcs.cloud/static/profile_placeholder.png' }} style={{ alignSelf: 'center', width: 200, height: 200, borderRadius: 100 }} />
                        <Button type={'secondary'} size={'medium'} onPress={handleChoosePhoto}>Выбрать из галереи</Button>
                        <Button type={'secondary'} size={'medium'} onPress={handleMakePhoto}>Сделать фото</Button>
                    </Option>
                </>
            }
            <View style={{ flex: 1 }}></View>
            <Button type='text' size='medium' onPress={() => setIsFeedbackOpened(true)} >Оценить сервис</Button>
            <Button type='text' size='medium' onPress={() => shareText('РестоРейт, десятки заведений с честными отзывами в 20 городах России. https://gitlab.com/resto-rate/app/-/releases')} >Рассказать о приложении</Button>
            {
                !user.Empty &&
                <Button type='secondary' size='large' onPress={logout} style={{ marginVertical: 16 }}>Выйти из аккаунта</Button>
            }
            <Text state='secondary' style={{ textAlign: 'center' }}>{displayName} {version}</Text>
            <Modal visible={isFeedbackOpened} animationType="slide" transparent>
                <TouchableOpacity activeOpacity={1} style={styles.modalOverlay} onPress={() => setIsFeedbackOpened(false)} />
                <View style={styles.modalContainer}>
                    <Text type={'h2'} state={'primary'}>Поделитесь Вашими впечатлениями</Text>
                    <Input value={feedback} onChangeText={setFeedback} autoFocus={true} style={{ minHeight: 200 }} multiline={true} />
                    <Button type='primary' size='large' onPress={() => setIsFeedbackOpened(false)}>Отправить</Button>
                    <Icons icon='Cross' color='contrast' size={24} padding={8} onPress={() => setIsFeedbackOpened(false)} style={{ position: 'absolute', top: 4, right: 4 }} />
                </View>
            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: Platform.OS === 'ios' ? 48 : 24,
        paddingHorizontal: 16,
    },
    citySelectContainer: {
        height: Dimensions.get('screen').height / 2,
        marginVertical: 28,
    },
    citySelectItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 16,
        borderBottomWidth: 1,
        borderBottomColor: Colors.Divider,
    },
    modalOverlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
    },
    modalContainer: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: Colors.Base0,
        width: '100%',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        padding: 16,
        gap: 8,
    },
})
