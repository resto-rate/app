import React, { useState } from 'react'
import { StyleSheet, View, TouchableOpacity, ScrollView, Image, GestureResponderEvent } from 'react-native'
import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import { User } from '../context/userContext'

import Review, { Review as ReviewType } from '../components/Profile/Review'
import Collection, { Collection as CollectionType } from '../components/Profile/Review'
import ImageCarousel from '../components/ImageCarousel'

import Colors from '../constants/Colors'


export default () => {
    const route = useRoute()
    const navigation = useNavigation<NativeStackNavigationProp<any>>()
    const [user, setUser] = useState<User | null>(null)
    const isFocused = useIsFocused()

    const [activeTab, setActiveTab] = useState('collections')
    const [userPhotoVisible, setUserPhotoVisible] = useState(false)

    const [collections, setCollections] = useState<{ Count: number, Collections: CollectionType[] }>({ Count: 0, Collections: [] })
    const [reviews, setReviews] = useState<{ Count: number, Reviews: ReviewType[] }>({ Count: 0, Reviews: [] })

    const [swipePage, setSwipePage] = useState(0)

    const userId = route.params.id

    const onTouchStart = (e: GestureResponderEvent) => {
        setSwipePage(e.nativeEvent.pageX)
    }

    const onTouchEnd = (e: GestureResponderEvent) => {
        if (swipePage - e.nativeEvent.pageX > 20 && activeTab === 'collections') {
            setActiveTab('reviews')
        }
        if (swipePage - e.nativeEvent.pageX < -20 && activeTab === 'reviews') {
            setActiveTab('collections')
        }
    }

    // const fetchData = async () => {
    //     if (!userId) {
    //         return
    //     }
    //     await getUserReviews(userId).then(resp => {
    //         if (resp.ok) {
    //             return resp.json()
    //         }
    //     }).then((body: { Count: number, Reviews: ReviewType[] }) => {
    //         if (body) {
    //             setReviews(body)
    //         }
    //     })
    // }

    // useEffect(() => {
    //     if (isFocused) {
    //         fetchData()
    //     }
    // }, [isFocused])
    return <></>

    return (
        <ScrollView style={styles.container} showsVerticalScrollIndicator={false} contentContainerStyle={{ alignItems: 'center' }}>
            <ImageCarousel images={[user.ImageURL]} visible={userPhotoVisible} imageIndex={0} onRequestClose={() => setUserPhotoVisible(false)} />
            <View style={styles.photo}>
                {
                    user.ImageURL ?
                        <TouchableOpacity onPress={() => setUserPhotoVisible(true)}><Image style={styles.photoImage} source={{ uri: user.ImageURL }} /></TouchableOpacity>
                        :
                        <Text style={styles.photoText}>{user.Name.charAt(0)}{user.Surname.charAt(0)}</Text>
                }
            </View>
            <Text type='h2' style={styles.name}>{user.Name} {user.Surname}</Text>
            <View style={styles.tabs}>
                <TouchableOpacity onPress={() => setActiveTab('collections')} style={[styles.tab, activeTab == 'collections' && styles.activeTab]}>
                    <Text state={activeTab == 'collections' ? 'active' : 'primary'} style={{ textAlign: 'center' }}>Подборки</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setActiveTab('reviews')} style={[styles.tab, activeTab == 'reviews' && styles.activeTab]}>
                    <Text state={activeTab == 'reviews' ? 'active' : 'primary'} style={{ textAlign: 'center' }}>Отзывы</Text>
                </TouchableOpacity>
            </View>
            <View onTouchStart={onTouchStart} onTouchEnd={onTouchEnd}>
                {
                    activeTab == 'collections' ?
                        <>
                            {
                                collections.Count > 0 ?
                                    <View style={styles.collections}>
                                        {collections.Collections.map((el) => <Collection key={el.id} {...el} />)}
                                    </View>
                                    :
                                    <View style={styles.placeholder}>
                                        <Text type='h2'>Пока пусто</Text>
                                        <Text type='sec1' state='secondary'>Здесь будут Ваши сохраненные заведения</Text>
                                    </View>
                            }
                        </>
                        :
                        <>
                            {
                                reviews.Count > 0 ?
                                    <View style={styles.reviews}>
                                        {reviews.Reviews.map((el, ind) => <Review key={el.ReviewGUID} review={el} page='profile' />)}
                                    </View>
                                    :
                                    <View style={styles.placeholder}>
                                        <Text type='h2'>Пока пусто</Text>
                                        <Text type='sec1' state='secondary'>Здесь будут оставленные Вами отзывы</Text>
                                    </View>
                            }
                        </>
                }
                <View style={{ flex: 1, backgroundColor: 'red' }} />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Background,
    },
    photo: {
        paddingHorizontal: 16,
        width: 100,
        height: 100,
        backgroundColor: Colors.Orange800,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 24,
    },
    photoImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    photoText: {
        color: Colors.Base0,
        fontSize: 32,
        fontWeight: '600',
    },
    name: {
        paddingHorizontal: 16,
        marginTop: 12,
    },
    tabs: {
        paddingHorizontal: 16,
        flexDirection: 'row',
        marginTop: 32,
    },
    tab: {
        width: '50%',
        paddingVertical: 4,
        borderBottomColor: Colors.Base400,
        borderBottomWidth: 1,
    },
    activeTab: {
        borderBottomColor: Colors.Orange800,
    },
    placeholder: {
        marginVertical: '40%',
        alignItems: 'center',
        gap: 8,
    },
    collections: {
        paddingHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        gap: 8,
        rowGap: 24,
        marginVertical: 16,
    },
    reviews: {
        gap: 24,
        marginVertical: 24,
    },
    placeholderContainer: {
        paddingHorizontal: 16,
        backgroundColor: Colors.Background,
        flex: 1,
        justifyContent: 'center',
    }
})
