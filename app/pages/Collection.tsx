import React, { useEffect, useState } from 'react'
import { View, ScrollView, StyleSheet, Modal, TouchableOpacity, Platform, StatusBar } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'

import { createCollection, getCollection } from '../api/establishment'
import { switchEnding } from '../utils/Functions'

import Text from '../components/Text'
import EateryCard, { EateryCardProps } from '../components/EateryCard'

import Emptybox from '../assets/images/emptybox.svg'

import Icons from '../components/Icons/Icons'
import Colors from '../constants/Colors'
import Input from '../components/Input'
import Button from '../components/Button'
import { useUserContext } from '../context/userContext'
import { useCollectionsContext } from '../context/collectionsContext'
import CreateUpdateCollection from '../components/CreateUpdateCollection'


type Collection = {
    CollectionGUID: string
    AccountGUID: string
    Title: string
    Preview: string
    EstablishmentsCount: 0
    Establishments: EateryCardProps[]
}

export default () => {
    const route = useRoute()
    const navigation = useNavigation()
    const [modalVisible, setModalVisible] = useState(false)

    const [collection, setCollection] = useState<Collection | null>(null)

    const [title, setTitle] = useState('')

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: collection === null ? 'Загрузка...' : collection.Title,
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerLeft: () => <Icons icon='ChevronLeft' color='default' size={24} padding={8} onPress={() => navigation.goBack()} />,
            headerTitle: ({ children }: any) => <Text type='h3'>{children}</Text>,
            headerRight: () => <Icons icon='Dots' color='default' size={24} padding={8} onPress={() => setModalVisible(true)} />,
        })
    }, [navigation, collection])

    const fetchData = async () => {
        const collectionId = route.params.id
        if (collectionId) {
            await getCollection(collectionId).then(resp => {
                if (resp.ok) {
                    return resp.json()
                }
            }).then(body => {
                if (body) {
                    setCollection(body)
                }
            })
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    useEffect(() => {
        if (modalVisible) {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.ModalOverlay)
            navigation.setOptions({ navigationBarColor: Colors.Base0 })
        } else {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.Background)
            navigation.setOptions({ navigationBarColor: Colors.Background })
        }
        if (modalVisible && collection) {
            setTitle(collection.Title)
        } else {
            setTitle('')
        }
    }, [modalVisible])

    if (collection === null) {
        return <></>
    }

    return (
        <>
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                {
                    collection.EstablishmentsCount === 0 ?
                        <View style={styles.emptybox}>
                            <Emptybox width={140} height={140} />
                            <Text type='h2'>Пока пусто</Text>
                            <Text type='sec1' state='secondary'>В этой коллекции еще нет заведений</Text>
                        </View>
                        :
                        <>
                            <Text type='h3' style={styles.heading}>{collection.EstablishmentsCount} {switchEnding(['мест', 'место', 'места'], collection.EstablishmentsCount)}</Text>
                            <View style={styles.collections}>
                                {
                                    collection.Establishments.map(eatery =>
                                        <EateryCard page='collection' key={eatery.EstablishmentGUID} {...eatery} />
                                    )
                                }
                            </View>
                        </>
                }
            </ScrollView>
            <CreateUpdateCollection modalVisible={modalVisible} setModalVisible={setModalVisible} title={title} setTitle={setTitle} collection={collection} setCollection={setCollection} />
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 24,
        marginBottom: 32,
    },
    heading: {
        paddingHorizontal: 16,
    },
    collections: {
        gap: 24,
        marginTop: 16,
    },
    emptybox: {
        gap: 8,
        alignItems: 'center',
        marginTop: '50%',
    },
})
