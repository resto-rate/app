import React, { useEffect, useRef, useState } from 'react'
import { StyleSheet, View, Image, Dimensions, ScrollView, StatusBar, Animated, Easing, Platform, ImageBackground, TouchableOpacity } from 'react-native'
import { useIsFocused, useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { SvgUri } from 'react-native-svg'

import { useUserContext } from '../../context/userContext'
import { getCollections, searchEstablishments } from '../../api/establishment'
import { switchEnding } from '../../utils/Functions'

import Button from '../../components/Button'
import Text from '../../components/Text'
import Collection from '../../components/Profile/Collection'
import EateryCardSmall, { EateryCardSmallProps } from '../../components/EateryCardSmall'

import Guide from '../../assets/images/guide.svg'

import Colors from '../../constants/Colors'
import { useCollectionsContext } from '../../context/collectionsContext'


const photos = [
    'https://www.foodandwine.com/thmb/DI29Houjc_ccAtFKly0BbVsusHc=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/crispy-comte-cheesburgers-FT-RECIPE0921-6166c6552b7148e8a8561f7765ddf20b.jpg',
    'https://media-cdn.tripadvisor.com/media/photo-p/13/d5/05/7b/photo0jpg.jpg',
    'https://popmenucloud.com/hbyodszv/765c964d-0043-4ca0-a1e2-1ab13aeaf400',
    'https://nomadsouthbay.com/wp-content/uploads/2021/12/restaurant-interior-private-parties.jpg',
    'https://images.squarespace-cdn.com/content/v1/5d716dd232e14f0001f76e46/1582497276058-J8C1S993XY52C4B520VZ/food+medley.jpg?format=1500w',
]

const popularCuisines = [
    {
        title: 'Вкус Италии',
        numberPlaces: 15,
        image: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Italy.jpg'
    },
    {
        title: 'Вкус Грузии',
        numberPlaces: 4,
        image: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Georgia.jpg'
    },
    {
        title: 'Вкус Азии',
        numberPlaces: 12,
        image: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Asia.jpg'
    },
]

const reasons = [
    {
        title: 'Романтический вечер',
        description: 'Места с особой атмосферой и предложениями для влюбленных',
        numberPlaces: 45,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Romantic.svg'
    },
    {
        title: 'Банкеты и мероприятия',
        description: 'Заведения для корпоративов, юбилеев и свадеб, которые вместят всех ваших гостей',
        numberPlaces: 45,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Party.svg'
    },
    {
        title: 'Встречи с друзьями',
        description: 'Заведения для корпоративов, юбилеев и свадеб, которые вместят всех ваших гостей',
        numberPlaces: 45,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Friends.svg'
    },
    {
        title: 'Обед с семьей',
        description: 'Места с детским меню и развлечениями',
        numberPlaces: 45,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Family.svg'
    },
]
9200882517
export default () => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    const { user, setUser } = useUserContext()
    const { collections, setCollections } = useCollectionsContext()
    const isFocused = useIsFocused()

    const [bestEateries, setBestEateries] = useState<EateryCardSmallProps[] | null>(null)

    const fetchData = async () => {
        await searchEstablishments(0, { city: user.City }).then(resp => {
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
                return null
            }
        }).then(body => {
            setBestEateries(body.Establishments)
        })
        await getCollections(user.AccountGUID).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            setCollections(body)
        })
    }

    useEffect(() => {
        fetchData()
    }, [user, isFocused])

    useEffect(() => {
        if (isFocused) {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.Orange800)
            StatusBar.setBarStyle('light-content')
        } else {
            Platform.OS == 'android' && StatusBar.setBackgroundColor(Colors.Background)
            StatusBar.setBarStyle('dark-content')
        }
    }, [isFocused])

    const tickerPosition = useRef(new Animated.Value(0)).current

    const startTicker = () => {
        Animated.loop(
            Animated.sequence([
                Animated.timing(tickerPosition, {
                    toValue: 1,
                    duration: 10000,
                    easing: Easing.linear,
                    useNativeDriver: false,
                }),
                Animated.timing(tickerPosition, {
                    toValue: 0,
                    duration: 0,
                    useNativeDriver: false,
                })
            ])
        ).start()
    }

    useEffect(() => {
        startTicker()
    }, [])

    return (
        <ScrollView style={styles.container}>
            <Animated.View style={[styles.ticker, { right: tickerPosition.interpolate({ inputRange: [0, 1], outputRange: ['0%', '50%'] }) }]}>
                {
                    photos.map((el, ind) =>
                        <Image key={ind} style={styles.tickerImage} source={{ uri: el.toString() }} />
                    )
                }
                {
                    photos.map((el, ind) =>
                        <Image key={ind} style={styles.tickerImage} source={{ uri: el.toString() }} />
                    )
                }
            </Animated.View>
            {
                user.Empty ?
                    <View style={styles.enterContainer}>
                        <Guide width={'50%'} />
                        <Text type={'h2'}>Составьте личный гид по местам</Text>
                        <Text type={'sec1'}>Авторизируйтесь, чтобы создавать персональные подборки и не потерять любимые заведения</Text>
                        <Button type={'primary'} size={'medium'} onPress={() => navigation.push('Auth')} style={{ marginTop: 8 }}>Войти</Button>
                    </View>
                    :
                    collections.Count > 0 ?
                        <View>
                            <Text type='h2' style={styles.sectionHeader}>Ваши подборки</Text>
                            <ScrollView contentContainerStyle={styles.collections} showsHorizontalScrollIndicator={false} horizontal={true}>
                                {
                                    collections.Collections.map((collection, index) =>
                                        <Collection key={index} {...collection} style={styles.collection} />
                                    )
                                }
                            </ScrollView>
                        </View>
                        :
                        <></>
            }
            {
                bestEateries !== null ?
                    <View style={{ paddingHorizontal: 16, marginTop: 32 }}>
                        <TouchableOpacity onPress={() => navigation.push('Settings')}><Text type={'h2'}>Лучшее в <Text type={'h2'} state={'active'}>вашем городе</Text></Text></TouchableOpacity>

                        <View style={styles.bestEstablishments}>
                            {
                                bestEateries.slice(0, 4).map((el, ind) => <EateryCardSmall key={ind} {...el} />)
                            }
                        </View>

                    </View>
                    :
                    <></>
            }
            <View style={{ paddingHorizontal: 16, marginTop: 48, gap: 8 }}>
                <Text type={'h2'} style={{ marginBottom: 8 }}>Популярные кухни</Text>
                {
                    popularCuisines.map((el, ind) =>
                        <TouchableOpacity key={ind} style={styles.cuisine}>
                            <Image source={{ uri: el.image }} resizeMode={'cover'} style={{ height: 120, borderRadius: 24 }} />
                            <View style={{ position: 'absolute', bottom: 16, left: 16 }}>
                                <Text type={'h3'} state={'contrast'}>{el.title}</Text>
                                <Text type={'sec1'} style={styles.cuisinePlaces}>{el.numberPlaces} {switchEnding(['мест', 'место', 'места'], el.numberPlaces)}</Text>
                            </View>
                        </TouchableOpacity>
                    )
                }
            </View>
            <View style={{ marginTop: 48 }}>
                <Text type={'h2'} style={{ paddingHorizontal: 16 }}>Места на любой случай</Text>
                <ScrollView contentContainerStyle={styles.collections} showsHorizontalScrollIndicator={false} horizontal={true}>
                    {
                        reasons.map((reason, index) =>
                            <TouchableOpacity key={index} style={{ padding: 16, backgroundColor: Colors.Surface4, width: 280, height: 280, borderRadius: 12, justifyContent: 'space-between' }}>
                                <View style={{ width: '100%' }}>
                                    <Text type='h3'>{reason.title}</Text>
                                    <Text type='sec1'>{reason.description}</Text>
                                </View>
                                <SvgUri uri={reason.photo} height={160} style={{ maxWidth: 200, alignSelf: 'flex-end' }} />
                            </TouchableOpacity>
                        )
                    }
                </ScrollView>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Background,
    },
    ticker: {
        paddingVertical: 32,
        flexDirection: 'row',
        gap: 8,
    },
    tickerImage: {
        width: 140,
        height: 90,
        borderRadius: 16,
    },
    sectionHeader: {
        paddingHorizontal: 16,
    },
    enterContainer: {
        backgroundColor: Colors.Surface4,
        marginHorizontal: 16,
        padding: 16,
        borderRadius: 12,
        gap: 8,
    },
    collections: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        flexDirection: 'row',
        gap: 8,
    },
    collection: {
        width: Dimensions.get('screen').width / 2.3,
    },
    bestEstablishments: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        rowGap: 16,
        columnGap: 8,
        marginTop: 16,
    },
    cuisine: {
        width: '100%',
        position: 'relative',
        borderRadius: 24
    },
    cuisinePlaces: {
        paddingVertical: 4,
        paddingHorizontal: 16,
        backgroundColor: Colors.Base100,
        borderRadius: 50,
        marginTop: 8,
        alignSelf: 'flex-start',
    },
})
