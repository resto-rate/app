import React from 'react'
import { Platform, TouchableOpacity, View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { SafeAreaView } from 'react-native'

import Text from '../../components/Text'

import Home from './Home';
import Profile from './Profile';
import Search from './Search';

import User from '../../components/Icons/User.svg'
import Star from '../../components/Icons/Star.svg'
import SearchIcon from '../../components/Icons/Search.svg'
import Icons from '../../components/Icons/Icons'

import Colors from '../../constants/Colors'


const Tabs = createBottomTabNavigator();

export default () => {
  const navigation = useNavigation<NativeStackNavigationProp<any>>()

  return (
    <>
      <Tabs.Navigator
        screenOptions={{
          tabBarStyle: {
            backgroundColor: Colors.Orange0,
            borderTopColor: Colors.Orange50,
            borderTopWidth: 1,
          },
          tabBarInactiveTintColor: Colors.Base500,
          tabBarActiveTintColor: Colors.Orange800,
          tabBarShowLabel: false,
          headerTitle: (props) => <Text type='h1'>{props.children}</Text>,
          headerStyle: { backgroundColor: Colors.Background },
          headerShadowVisible: false,
          headerTitleAlign: 'left',
          headerRightContainerStyle: {
            padding: 16
          },
          tabBarHideOnKeyboard: true,
        }}
        initialRouteName='Home'
      >
        <Tabs.Screen
          name="Profile"
          component={Profile}
          options={{
            title: 'Профиль',
            tabBarIcon: (props) => <User fill={props.color} />,
            headerRight: () => <Icons icon='Gear' color={'default'} size={24} padding={8} onPress={() => navigation.push('Settings')} />,
          }}
        />
        <Tabs.Screen
          name="Home"
          component={Home}
          options={{
            title: 'РестоРейт',
            tabBarIcon: (props) => <Star width={28} height={28} fill={props.color} />,
            headerTitle: 'РестоРейт',
            header: (props) =>
              <>
                {Platform.OS == 'ios' && <SafeAreaView style={{ backgroundColor: Colors.Orange800 }} />}
                <View style={{ padding: 16, paddingTop: 8, gap: 16, backgroundColor: Colors.Orange800 }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center', gap: 8 }}>
                    <Star width={40} height={40} fill={Colors.Base0} />
                    <Text state='contrast' type='h1' style={{ fontWeight: '600' }}>{props.options.headerTitle}</Text>
                  </View>
                  <TouchableOpacity onPress={() => navigation.push('Tabs', { screen: 'Search' })} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor: Colors.Background, paddingHorizontal: 10, paddingVertical: 8, borderRadius: 8, width: '100%' }}>
                    <Text type='sec1' state='placeholder'>Поиск по заведениям</Text>
                    <SearchIcon width={16} height={16} fill={'#E79A77'} />
                  </TouchableOpacity>
                </View>
              </>
            ,
          }}
        />
        <Tabs.Screen
          name="Search"
          component={Search}
          options={{
            title: 'Поиск',
            tabBarIcon: (props) => <SearchIcon fill={props.color} />,
          }}
        />
      </Tabs.Navigator>
    </>
  )
}
