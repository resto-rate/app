import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, TextInput, TouchableOpacity, View } from 'react-native'
import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import { searchEstablishments } from '../../api/establishment'

import Text from '../../components/Text'
import EateryCard, { EateryCardProps } from '../../components/EateryCard'

import Icons from '../../components/Icons/Icons'

import Colors from '../../constants/Colors'
import AsyncStorage from '@react-native-async-storage/async-storage'


export default () => {
    const isFocused = useIsFocused()
    const [query, setQuery] = useState('')
    const navigation = useNavigation<NativeStackNavigationProp<any>>()


    const [eateries, setEateries] = useState<{ Count: number, Establishments: EateryCardProps[], Query: string } | null>(null)

    const searchQuery = async () => {
        AsyncStorage.getItem('params').then(async p => {
            console.log(p)
            if (p !== null) {
                let params = JSON.parse(p)
                params.fulltextfind = query
                await searchEstablishments(0, params).then(resp => {
                    if (resp.ok) {
                        return resp.json()
                    }
                }).then(body => {
                    if (body) {
                        body.Query = query
                        setEateries(body)
                    }
                })
            }
        })
    }

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerBackVisible: false,
            header: () =>
                <View style={{ paddingHorizontal: 16, height: 56, alignItems: 'center', flexDirection: 'row', backgroundColor: Colors.Background }}>
                    <Icons icon='Filter' color='default' size={24} padding={8} onPress={() => navigation.push('Filters')} />
                    <TextInput value={query} onChangeText={setQuery} onSubmitEditing={searchQuery} autoFocus={true} placeholder={'Поиск по заведениям'} placeholderTextColor={Colors.Base500} cursorColor={Colors.Base800} style={{ flex: 1, paddingHorizontal: 16, color: Colors.Base800, fontSize: 18 }} />
                    <Icons icon='Cross' color='default' size={24} padding={8} onPress={() => setQuery('')} />
                </View>
        });
    }, [navigation, query])

    useEffect(() => {
        searchQuery()
    }, [isFocused])

    return (
        <ScrollView style={styles.container}>
            {
                eateries === null ?
                    <Text style={{ padding: 16 }} type='h2'>Введите запрос</Text>
                    :
                    <>
                        <Text style={{ padding: 16 }} type='h2'>{eateries.Query.length > 0 ? `Заведения по запросу «${eateries.Query}»` : 'Заведения города'}</Text>
                        <View style={{ gap: 24 }}>
                            {
                                eateries.Establishments.map((el, ind) => <EateryCard page='search' {...el} key={ind} />)
                            }
                        </View>
                    </>
            }
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Background,
    },
})
