import React, { useEffect, useState } from 'react'
import { View, ScrollView, StyleSheet, TextInput } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import { getEstablishmentMenu } from '../../api/establishment'

import Text from '../../components/Text'
import MealComponent, { Meal } from '../../components/Meal'
import Header from '../../components/Eatery/Header'

import Icons from '../../components/Icons/Icons'

import Colors from '../../constants/Colors'


export type Menu = {
    Count: number
    DishesByCategory: {
        Category: string
        Dishes: Meal[]
    }[]
}

type MenuCategoryProps = {
    title: string
    items: Meal[]
    query: string
}

const search = (what: string, where: string[]) => {
    let res = false
    what = what.toLowerCase()
    where.forEach(el => {
        if (el.toLowerCase().includes(what)) {
            res = true
        }
    })
    return res
}

const filter = (what: string, items: Meal[]) => {
    return items.filter(el => {
        const text = el.Composition.split(' ').concat([el.DishName])
        if (search(what, text)) {
            return el
        }
    })
}

const MenuCategory = (props: MenuCategoryProps) => {
    const filtered = filter(props.query, props.items)

    const searchCategory = props.title.toLowerCase().includes(props.query.toLowerCase())

    if (props.query !== '' && filtered.length === 0 && !searchCategory) {
        return <></>
    }

    return (
        <View style={styles.category}>
            <Text type='h2'>{props.title}</Text>
            <View style={styles.categoryMeals}>
                {
                    props.query !== '' && !searchCategory ?
                        filtered.map(el => <MealComponent key={el.DishGUID} meal={el} />)
                        :
                        props.items.map(el => <MealComponent key={el.DishGUID} meal={el} />)
                }
            </View>
        </View>
    )
}


export default () => {
    const route = useRoute()
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    const establishmentId = route.params.id

    const [menu, setMenu] = useState<Menu | null>(null)
    const [query, setQuery] = useState('')
    const [searchActive, setSearchActive] = useState(false)

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Меню',
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            header: () =>
                <View style={{ paddingHorizontal: 16, height: 56, alignItems: 'center', flexDirection: 'row', backgroundColor: Colors.Background }}>
                    <Icons icon='ChevronLeft' color='default' size={24} padding={8} onPress={() => navigation.goBack()} />
                    {
                        searchActive ?
                            <TextInput value={query} onChangeText={setQuery} onSubmitEditing={() => 0} autoFocus={true} placeholder={'Поиск по меню'} placeholderTextColor={Colors.Base500} cursorColor={Colors.Base800} style={{ flex: 1, paddingHorizontal: 16, color: Colors.Base800, fontSize: 18 }} />
                            :
                            <Text type='h3' style={{ flex: 1, textAlign: 'center' }}>Меню</Text>
                    }
                    {
                        searchActive ?
                            <Icons icon='Cross' color='default' size={24} padding={8} onPress={() => { setSearchActive(false); setQuery('') }} />
                            :
                            <Icons icon='Search' color='default' size={24} padding={8} onPress={() => setSearchActive(true)} />
                    }
                </View>,
        })
    }, [navigation, searchActive, query])

    const fetchMenu = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getEstablishmentMenu(establishmentId).then(resp => {
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                setMenu({ ...body })
            } else {
                console.log('Wrong establishment id')
            }
        })
    }

    useEffect(() => {
        fetchMenu()
    }, [])

    if (menu === null)
        return <Text>Загрузка...</Text>

    return (
        <ScrollView style={styles.container}>
            <Header Establishment={establishmentId} />
            {/*<MenuTabs categories={menu.map(el => el.title)} />*/}
            <View style={styles.categories}>
                {
                    menu.Count ?
                        menu.DishesByCategory.sort().map(el => <MenuCategory key={el.Category} title={el.Category} items={el.Dishes} query={query} />)
                        :
                        <></>
                }
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {

    },
    categories: {
        gap: 24,
        marginVertical: 24,
        paddingHorizontal: 16,
    },
    category: {
        gap: 16
    },
    categoryMeals: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        rowGap: 16,
        columnGap: 8,
    }
})
