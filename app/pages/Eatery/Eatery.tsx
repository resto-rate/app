import React, { useState, useEffect, useRef } from 'react'
import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { View, ScrollView, StyleSheet, TouchableOpacity, Image, Dimensions, Linking, Animated, GestureResponderEvent } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { getEstablishment, getEstablishmentEvents, getEstablishmentPromotions } from '../../api/establishment'
import { openGps, prettyPhone, prettyTime, shareText } from '../../utils/Functions'

import Text from '../../components/Text'
import MealComponent, { Meal } from '../../components/Meal'
import Button from '../../components/Button'
import RatingCard, { RatingCardProps } from '../../components/Eatery/RatingCard'
import Header from '../../components/Eatery/Header'
import EventCard from '../../components/EventCard'
import HeaderChips from '../../components/Eatery/HeaderChips'

import Icons from '../../components/Icons/Icons'
import Colors from '../../constants/Colors'


const images = [
    'https://www.foodandwine.com/thmb/DI29Houjc_ccAtFKly0BbVsusHc=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/crispy-comte-cheesburgers-FT-RECIPE0921-6166c6552b7148e8a8561f7765ddf20b.jpg',
    'https://media-cdn.tripadvisor.com/media/photo-p/13/d5/05/7b/photo0jpg.jpg',
    'https://popmenucloud.com/hbyodszv/765c964d-0043-4ca0-a1e2-1ab13aeaf400',
    'https://nomadsouthbay.com/wp-content/uploads/2021/12/eateryaurant-interior-private-parties.jpg',
]

type EateryDay = {
    OpenTime: number
    CloseTime: number
    eateryDay: boolean
}

export type Eatery = {
    CollectionGUIDs: string[]
    EstablishmentGUID: string
    EstablishmentTitle: string
    Geolocation: {
        Latitude: number
        Longitude: number
    },
    EstablishmentLogo: string
    Type: string
    Cuisines: string[]
    Images: string[]
    Rating: RatingCardProps
    AvgBill: number
    Description: string
    Address: string
    Contacts: {
        MobilePhone: string
        SiteURL: string
        WhatsApp: string
        Telegram: string
        VK: string
    }
    Schedule: {
        Monday: EateryDay
        Tuesday: EateryDay
        Wednesday: EateryDay
        Thursday: EateryDay
        Friday: EateryDay
        Saturday: EateryDay
        Sunday: EateryDay
    }
    FavDishes: Meal[]
    AdditionalServices: string[]
    SuitableCases: string[]
    AdminGUIDs: string[]
}

export type EateryPromotion = {
    PromotionGUID: string
    EstablishmentGUID: string
    Title: string
    Image: string
    Conditions: string
    StartTime: number
    EndTime: number
    ContactInfo: string
    Undying: boolean
}

export type EateryEvent = {
    EventGUID: string
    EstablishmentGUID: string
    EstablishmentTitle: string
    Title: string
    Image: string
    Description: string
    DateTime: number
    ContactInfo: string
}

export default () => {
    const route = useRoute()
    const navigation = useNavigation<NativeStackNavigationProp<any>>()
    const isFocused = useIsFocused()

    const [eatery, setEatery] = useState<Eatery | null>(null)
    const [promotions, setPromotions] = useState<{ Count: number, Promotions: EateryPromotion[] } | null>(null)
    const [events, setEvents] = useState<{ Count: number, Events: EateryEvent[] } | null>(null)

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: eatery ? eatery.EstablishmentTitle : 'Загрузка...',
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerLeft: () => <Icons icon='ChevronLeft' color='default' size={24} padding={8} onPress={() => {
                let parent = navigation.getParent()
                if (parent !== undefined) {
                    navigation.goBack();
                } else {
                    navigation.navigate('Home')
                }
            }} />,
            headerTitle: ({ children }) => <Text type='h3'>{children}</Text>,
            headerRight: () =>
                <View style={{ flexDirection: 'row' }}>
                    <Icons icon='Share' color='default' size={24} padding={8} onPress={() => shareText(`https://web.restorate.space/rest/${eatery?.EstablishmentGUID}`)} style={{ paddingRight: 6 }} />
                    
                    <Icons icon='HeartOutline' color='default' size={24} padding={8} onPress={() => navigation.push('SaveToCollection', { id: eatery?.EstablishmentGUID })} style={{ paddingLeft: 6 }} />
                </View>
        });
    }, [navigation, eatery])

    const fetchEstablishment = async () => {
        const establishmentId = route.params.id

        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getEstablishment(establishmentId).then(resp => {
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                setEatery({ ...body })
            } else {
                console.log('Wrong establishment id')
            }
        })

        await getEstablishmentPromotions(establishmentId).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                setPromotions(body)
            }
        })

        await getEstablishmentEvents(establishmentId).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                setEvents(body)
            }
        })
    }

    useEffect(() => {
        fetchEstablishment()
    }, [isFocused])

    const [activeImage, setActiveImage] = useState(0)
    const [scheduleShown, setScheduleShown] = useState(false)

    let imagesWidth = Array<Animated.Value>(images.length)
    imagesWidth[0] = useRef(new Animated.Value(Dimensions.get('screen').width - (images.length - 1) * 36 - 28)).current
    for (let i = 1; i < images.length; i++) {
        imagesWidth[i] = useRef(new Animated.Value(32)).current
    }

    const spread = (index: number) => {
        Animated.timing(imagesWidth[index], {
            toValue: Dimensions.get('screen').width - (images.length - 1) * 36 - 28,
            duration: 100,
            useNativeDriver: false,
        }).start()
    }

    const squeeze = (index: number) => {
        Animated.timing(imagesWidth[index], {
            toValue: 32,
            duration: 100,
            useNativeDriver: false,
        }).start()
    }

    useEffect(() => {
        for (let i = 0; i < images.length; i++) {
            if (i == activeImage) {
                spread(i)
            } else {
                squeeze(i)
            }
        }
    }, [activeImage])

    const [swipe, setSwipe] = useState(0)
    const onTouchStart = (e: GestureResponderEvent) => {
        setSwipe(e.nativeEvent.pageX)
    }

    const onTouchEnd = (e: GestureResponderEvent) => {
        if (swipe - e.nativeEvent.pageX > 20 && activeImage < 3) {
            setActiveImage(activeImage + 1)
        }
        if (swipe - e.nativeEvent.pageX < -20 && activeImage > 0) {
            setActiveImage(activeImage - 1)
        }
    }

    if (eatery === null)
        return <></>

    return (
        <ScrollView>
            <Header Establishment={eatery.EstablishmentGUID} />
            <HeaderChips type={eatery.Type} cuisines={eatery.Cuisines} />
            <View style={styles.section}>
                <Text type='h2'>Фото заведения</Text>
                <View style={{ flexDirection: 'row', gap: 4, marginTop: 4 }} onTouchStart={onTouchStart} onTouchEnd={onTouchEnd}>
                    {
                        eatery.Images.slice(0, 4).map((image, index) => (
                            <TouchableOpacity key={index} activeOpacity={1} onPress={() => setActiveImage(index)}>
                                <Animated.View style={{ width: imagesWidth[index] }}>
                                    <Image style={{ borderRadius: 16, height: 220 }} source={{ uri: image }} />
                                </Animated.View>
                            </TouchableOpacity>
                        ))
                    }
                </View>
                <Button style={{ alignSelf: 'flex-end' }} onPress={() => navigation.push('EateryPhotos', { id: eatery.EstablishmentGUID })} size='medium' type='text'>Все фотографии</Button>
            </View>
            {
                eatery.FavDishes.length > 0 ?
                    <View style={styles.section}>
                        <Text style={{ marginTop: 4 }} type='h2'>Лучшие блюда</Text>
                        <View style={styles.menu}>
                            {eatery.FavDishes.map(el => <MealComponent key={el.DishGUID} meal={el} />)}
                        </View>
                        <Button style={{ alignSelf: 'flex-end' }} onPress={() => navigation.push('EateryMenu', { id: eatery.EstablishmentGUID })} size='medium' type='text'>Открыть меню</Button>
                    </View>
                    :
                    <></>
            }
            <View style={styles.section}>
                <Text style={{ marginTop: 4 }} type='h2'>Особенности заведения</Text>
                <Text>{eatery.Description}</Text>
                {
                    eatery.SuitableCases.concat(eatery.AdditionalServices).map((el, ind) =>
                        <View key={ind} style={styles.feature}>
                            <View style={styles.sqare} />
                            <Text>{el}</Text>
                        </View>
                    )
                }
            </View>
            <View style={{ marginTop: 24, paddingHorizontal: 16, gap: 12 }}>
                <Text style={{ marginTop: 4 }} type='h2'>Контакты</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', gap: 8 }}>
                    <Icons icon='Geo' color='default' size={14} padding={0} />
                    <Text>{eatery.Address}</Text>
                </View>

                <View style={{ gap: scheduleShown ? 8 : 0 }}>
                    <TouchableOpacity onPress={() => setScheduleShown(!scheduleShown)} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', gap: 8 }}>
                            <Icons icon='Time' color='default' size={14} padding={0} />
                            <Text>Расписание</Text>
                        </View>
                        <Icons icon='ChevronDown' color='default' size={24} padding={0} style={{ transform: [{ rotateX: scheduleShown ? '180deg' : '0deg' }] }} />
                    </TouchableOpacity>
                    <View style={{ height: scheduleShown ? 'auto' : 0 }}>
                        {
                            [
                                { day: 'Пн', time: eatery.Schedule.Monday.eateryDay ? 'Выходной' : `${prettyTime(eatery.Schedule.Monday.OpenTime)}-${prettyTime(eatery.Schedule.Monday.CloseTime)}` },
                                { day: 'Вт', time: eatery.Schedule.Tuesday.eateryDay ? 'Выходной' : `${prettyTime(eatery.Schedule.Tuesday.OpenTime)}-${prettyTime(eatery.Schedule.Tuesday.CloseTime)}` },
                                { day: 'Ср', time: eatery.Schedule.Wednesday.eateryDay ? 'Выходной' : `${prettyTime(eatery.Schedule.Wednesday.OpenTime)}-${prettyTime(eatery.Schedule.Wednesday.CloseTime)}` },
                                { day: 'Чт', time: eatery.Schedule.Thursday.eateryDay ? 'Выходной' : `${prettyTime(eatery.Schedule.Thursday.OpenTime)}-${prettyTime(eatery.Schedule.Thursday.CloseTime)}` },
                                { day: 'Пт', time: eatery.Schedule.Friday.eateryDay ? 'Выходной' : `${prettyTime(eatery.Schedule.Friday.OpenTime)}-${prettyTime(eatery.Schedule.Friday.CloseTime)}` },
                                { day: 'Сб', time: eatery.Schedule.Saturday.eateryDay ? 'Выходной' : `${prettyTime(eatery.Schedule.Saturday.OpenTime)}-${prettyTime(eatery.Schedule.Saturday.CloseTime)}` },
                                { day: 'Вс', time: eatery.Schedule.Sunday.eateryDay ? 'Выходной' : `${prettyTime(eatery.Schedule.Sunday.OpenTime)}-${prettyTime(eatery.Schedule.Sunday.CloseTime)}` }
                            ].map((el, index) =>
                                <View key={index} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text>{el.day}</Text>
                                    <Text numberOfLines={1} style={{ flex: 10 }}>....................................................................................................................</Text>
                                    <Text>{el.time}</Text>
                                </View>
                            )
                        }
                    </View>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', gap: 8 }}>
                    <Icons icon='Phone' color='default' size={14} padding={0} />
                    <TouchableOpacity onPress={() => Linking.openURL(`tel:+7${eatery.Contacts.MobilePhone}`)}><Text>{prettyPhone(eatery.Contacts.MobilePhone)}</Text></TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', gap: 8 }}>
                    <Icons icon='Network' color='default' size={14} padding={0} />
                    <TouchableOpacity onPress={() => Linking.openURL(eatery.Contacts.SiteURL)}><Text>{eatery.Contacts.SiteURL}</Text></TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => openGps(eatery.Geolocation, eatery.EstablishmentTitle, eatery.Address)}>
                    <Image
                        style={{ width: '100%', height: 'auto', aspectRatio: 65 / 40, flex: 1, borderRadius: 8 }}
                        source={{ uri: `https://static-maps.yandex.ru/v1?apikey=56248e87-b98a-444a-b8c7-0774f0a449df&lang=ru_RU&pt=${eatery.Geolocation.Longitude},${eatery.Geolocation.Latitude},pm2rdl&z=15&size=650,400` }} />
                </TouchableOpacity>
            </View>
            <RatingCard {...eatery.Rating} />
            {
                promotions !== null && promotions.Count !== 0 ?
                    <View style={{ marginBottom: 24, gap: 12 }}>
                        <Text type='h2' style={{ marginHorizontal: 16 }}>Акции</Text>
                        <ScrollView contentContainerStyle={{ gap: 8, paddingHorizontal: 16 }} showsHorizontalScrollIndicator={false} horizontal={true}>
                            {
                                promotions.Promotions.map((el, ind) =>
                                    <TouchableOpacity key={ind} onPress={() => AsyncStorage.setItem('promo', JSON.stringify(promotions.Promotions[ind])).then(() => navigation.push('Promotion'))}>
                                        <Image style={{ width: 108, height: 192, borderRadius: 8 }} source={{ uri: el.Image.length > 0 ? el.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/promotion_placeholder.jpg' }} />
                                    </TouchableOpacity>
                                )
                            }
                        </ScrollView>
                    </View>
                    :
                    <></>
            }
            {
                events !== null && events.Count !== 0 ?
                    <View style={{ marginVertical: 24, gap: 12 }}>
                        <Text type='h2' style={{ marginHorizontal: 16 }}>Мероприятия</Text>
                        <ScrollView contentContainerStyle={{ gap: 8, paddingHorizontal: 16 }} showsHorizontalScrollIndicator={false} horizontal={true}>
                            {events.Events.map(el => <EventCard key={el.EventGUID} {...el} />)}
                        </ScrollView>
                    </View>
                    :
                    <></>
            }

            <View style={{ marginBottom: 24 }}></View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    section: {
        marginTop: 24,
        paddingHorizontal: 16,
        gap: 8,
    },
    menu: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        gap: 8,
    },
    feature: {
        flexDirection: 'row',
        gap: 4,
        alignItems: 'center',
    },
    sqare: {
        backgroundColor: Colors.Orange800,
        width: 8,
        height: 8,
        borderRadius: 4,
    },
})
