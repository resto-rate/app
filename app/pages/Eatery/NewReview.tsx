import React, { useEffect, useState } from 'react'
import { Image, ScrollView, StyleSheet, Touchable, TouchableOpacity, View } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { launchImageLibrary, Asset } from 'react-native-image-picker'

import { createReview, uploadImage } from '../../api/establishment'
import { useUserContext } from '../../context/userContext'

import Text from '../../components/Text'
import Icons from '../../components/Icons/Icons'
import Header from '../../components/Eatery/Header'

import Star from '../../components/Icons/Star.svg'
import Colors from '../../constants/Colors'
import Input from '../../components/Input'
import Button from '../../components/Button'


export type NewReview = {
    LikedTheMost: string
    NeedToBeChanged: string
    Comment: string
    Rating: {
        Service: number
        Food: number
        Vibe: number
        PriceQuality: number
        WaitingTime: number
    },
    Images: string[]
}

export default () => {
    const route = useRoute()
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    const { user } = useUserContext()
    const establishmentId = route.params.id

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Поделитесь впечатлениями',
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerTitle: ({ children }) => <Text type='h3' state='active' style={{ flex: 1 }}>{children}</Text>,
            headerRight: () => <Icons icon='Cross' color='default' size={24} padding={8} onPress={() => navigation.goBack()} />,
        });
    }, [navigation])


    const [service, setService] = useState(0)
    const [vibe, setVibe] = useState(0)
    const [food, setFood] = useState(0)
    const [waitingTime, setWaitingTime] = useState(0)
    const [priceQuality, setPriceQuality] = useState(0)

    const [like, setLike] = useState('')
    const [dislike, setDislike] = useState('')
    const [comment, setComment] = useState('')

    const [photos, setPhotos] = useState<string[]>([])


    const [newPhoto, setNewPhoto] = useState<Asset | undefined>(undefined)

    const handleChoosePhoto = async () => {
        const result = await launchImageLibrary({ mediaType: 'photo' });
        if (!result.errorCode && !result.didCancel) {
            setNewPhoto(result.assets![0])
        } else {
            console.log(result)
        }
    }

    const sendNewPhoto = async () => {
        let formData = new FormData()
        if (newPhoto) {
            formData.append("imageFile", { ...newPhoto, name: newPhoto.fileName })
            try {
                const result = await uploadImage(formData, user.At.Token)
                const data = await result.json()
                setPhotos(photos.concat([data]))
                return true
            } catch (error) {
                console.error(error)
                return false
            }
        }
        return false
    }

    useEffect(() => {
        sendNewPhoto()
    }, [newPhoto])

    const sendReview = async () => {
        const body: NewReview = {
            LikedTheMost: like,
            NeedToBeChanged: dislike,
            Comment: comment,
            Rating: {
                Service: service,
                Food: food,
                Vibe: vibe,
                PriceQuality: priceQuality,
                WaitingTime: waitingTime
            },
            Images: photos
        }

        if (service === 0 || food === 0 || vibe === 0 || priceQuality === 0 || waitingTime === 0) {
            return
        }

        await createReview(body, establishmentId, user.At.Token).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            navigation.goBack()
        })
    }

    return (
        <ScrollView>
            <Header Establishment={establishmentId} />
            <View style={styles.container}>
                <Text type='h2' state='primary'>Оценки по критериям</Text>
                <View style={styles.rate}>
                    <Text type='h3' state='primary'>Обслуживание</Text>
                    <View style={{ flexDirection: 'row', gap: 4 }}>
                        {
                            [1, 2, 3, 4, 5].map(el =>
                                <TouchableOpacity key={el} activeOpacity={1} onPress={() => setService(el)}>
                                    <Star fill={service >= el ? Colors.Star : Colors.Base500} width={40} height={40}></Star>
                                </TouchableOpacity>
                            )
                        }
                    </View>
                </View>
                <View style={styles.rate}>
                    <Text type='h3' state='primary'>Атмосфера</Text>
                    <View style={{ flexDirection: 'row', gap: 4 }}>
                        {
                            [1, 2, 3, 4, 5].map(el =>
                                <TouchableOpacity key={el} activeOpacity={1} onPress={() => setVibe(el)}>
                                    <Star fill={vibe >= el ? Colors.Star : Colors.Base500} width={40} height={40}></Star>
                                </TouchableOpacity>
                            )
                        }
                    </View>
                </View>
                <View style={styles.rate}>
                    <Text type='h3' state='primary'>Питание</Text>
                    <View style={{ flexDirection: 'row', gap: 4 }}>
                        {
                            [1, 2, 3, 4, 5].map(el =>
                                <TouchableOpacity key={el} activeOpacity={1} onPress={() => setFood(el)}>
                                    <Star fill={food >= el ? Colors.Star : Colors.Base500} width={40} height={40}></Star>
                                </TouchableOpacity>
                            )
                        }
                    </View>
                </View>
                <View style={styles.rate}>
                    <Text type='h3' state='primary'>Время ожидания</Text>
                    <View style={{ flexDirection: 'row', gap: 4 }}>
                        {
                            [1, 2, 3, 4, 5].map(el =>
                                <TouchableOpacity key={el} activeOpacity={1} onPress={() => setWaitingTime(el)}>
                                    <Star fill={waitingTime >= el ? Colors.Star : Colors.Base500} width={40} height={40}></Star>
                                </TouchableOpacity>
                            )
                        }
                    </View>
                </View>
                <View style={styles.rate}>
                    <Text type='h3' state='primary'>Цена - качество</Text>
                    <View style={{ flexDirection: 'row', gap: 4 }}>
                        {
                            [1, 2, 3, 4, 5].map(el =>
                                <TouchableOpacity key={el} activeOpacity={1} onPress={() => setPriceQuality(el)}>
                                    <Star fill={priceQuality >= el ? Colors.Star : Colors.Base500} width={40} height={40}></Star>
                                </TouchableOpacity>
                            )
                        }
                    </View>
                </View>
                <View style={styles.rate}>
                    <Text type='h2'>Что понравилось?</Text>
                    <Input value={like} onChangeText={setLike} placeholder='Расскажите, за что похвалите заведение' style={styles.input} multiline={true} />
                </View>
                <View style={styles.rate}>
                    <Text type='h2'>Что стоит изменить?</Text>
                    <Input value={dislike} onChangeText={setDislike} placeholder='Опишите, что вам не понравилось' style={styles.input} multiline={true} />
                </View>
                <View style={styles.rate}>
                    <Text type='h2'>Комментарий</Text>
                    <Input value={comment} onChangeText={setComment} placeholder='Поделитесь впечатлениями' style={styles.input} multiline={true} />
                </View>
                <View style={styles.rate}>
                    <Text type='h2'>Фотографии</Text>
                </View>
            </View>
            <ScrollView contentContainerStyle={{ gap: 8, paddingHorizontal: 16, marginBottom: 24 }} showsHorizontalScrollIndicator={false} horizontal={true}>
                <Icons icon='AddPhoto' color='active' size={32} padding={34} style={{ backgroundColor: Colors.Orange0, borderRadius: 16 }} onPress={handleChoosePhoto} />
                {
                    photos.map((el, ind) =>
                        <View key={ind} style={{ position: 'relative' }}>
                            <Image style={styles.photo} source={{ uri: el }} />
                            <Icons icon='Cross' color='default' size={16} padding={4} style={{ backgroundColor: Colors.Base0, position: 'absolute', top: 4, right: 4, borderRadius: 12 }} onPress={() => { const newPhotos = photos.splice(ind + 1, 1); setPhotos(newPhotos) }} />
                        </View>
                    )
                }
            </ScrollView>
            <View style={styles.container}>
                <View style={styles.rate}>
                    <Button type='primary' size='large' onPress={sendReview}>Оставить отзыв</Button>
                </View>
            </View>
        </ScrollView >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 16,
        paddingBottom: 16,
        gap: 24,
    },
    rate: {
        gap: 12,
    },
    input: {
        minHeight: 100,
    },
    photo: {
        width: 100,
        height: 100,
        borderRadius: 16,
    },
})
