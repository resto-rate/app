import React, { useEffect, useState } from 'react'
import { View, ScrollView, TouchableOpacity, Image, Dimensions, StyleSheet } from 'react-native'
import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import { switchCriterion, switchEnding } from '../../utils/Functions'
import { getEstablishment, getEstablishmentReviews } from '../../api/establishment'
import { Eatery } from './Eatery'

import Text from '../../components/Text'
import Rating from '../../components/Rating'
import Header from '../../components/Eatery/Header'
import Review, { Review as ReviewType } from '../../components/Profile/Review'
import ImageCarousel from '../../components/ImageCarousel'

import Icons from '../../components/Icons/Icons'

import Colors from '../../constants/Colors'


export default () => {
    const route = useRoute()
    const navigation = useNavigation<NativeStackNavigationProp<any>>()
    const isFocused = useIsFocused()

    const [eatery, setEatery] = useState<Eatery | null>(null)
    const [reviews, setReviews] = useState<{ Count: number, Reviews: ReviewType[] }>({ Count: 0, Reviews: [] })
    const [imageView, setImageView] = useState({ imageIndex: 0, visible: false })

    const establishmentId = route.params.id

    const fetchData = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }
        await getEstablishment(establishmentId).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            setEatery(body)
        })
        await getEstablishmentReviews(establishmentId).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                setReviews(body)
            }
        })
    }

    useEffect(() => {
        fetchData()
    }, [isFocused])

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Отзывы',
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerLeft: () => <Icons icon='ChevronLeft' color='default' size={24} padding={8} onPress={() => navigation.goBack()} />,
            headerTitle: ({ children }) => <Text type='h3'>{children}</Text>,
            headerRight: () => <Icons icon='Plus' color='default' size={24} padding={8} onPress={() => navigation.push('NewReview', { id: establishmentId })} />,
        })
    }, [navigation])

    if (eatery === null) {
        return <Text style={{ paddingHorizontal: 16 }}>Загрузка...</Text>
    }

    return (
        <ScrollView>
            <ImageCarousel images={eatery.Rating.Images} {...imageView} onRequestClose={() => setImageView({ imageIndex: 0, visible: false })} />
            <Header Establishment={eatery} />
            <ScrollView contentContainerStyle={{ gap: 8, paddingHorizontal: 12, paddingTop: 8 }} showsHorizontalScrollIndicator={false} horizontal={true}>
                {
                    eatery.Rating.Images.map((el, ind) =>
                        <TouchableOpacity key={el} onPress={() => setImageView({ imageIndex: ind, visible: true })}>
                            <Image style={styles.headerPhoto} source={{ uri: el.toString() }} />
                        </TouchableOpacity>
                    )
                }
            </ScrollView>
            <View style={{ paddingHorizontal: 16, marginTop: 24, gap: 12 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'baseline' }}>
                    <Text type='h3'>Оценки по критериям</Text>
                    <Text type='sec2' state='secondary'>{eatery.Rating.ReviewCount} {switchEnding(['оценок', 'оценка', 'оценки'], eatery.Rating.ReviewCount)}</Text>
                </View>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', gap: 8 }}>
                    {
                        Object.keys(eatery.Rating.AvgRates).map((criterion, index) =>
                            <Rating key={index} rate={eatery.Rating.AvgRates[criterion]} text={switchCriterion(criterion.toLowerCase())} size='large' style={{ width: (Dimensions.get('screen').width - 40) / 2 }} />)
                    }
                    <TouchableOpacity onPress={() => navigation.push('NewReview', { id: establishmentId })} style={{ borderRadius: 8, backgroundColor: Colors.Orange800, paddingVertical: 4, paddingHorizontal: 8, width: (Dimensions.get('screen').width - 40) / 2 }}>
                        <Icons icon='Plus' size={24} padding={0} color='contrast' />
                        <Text state='contrast'>Оценить</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ marginVertical: 24, marginHorizontal: 16, height: 1, backgroundColor: Colors.Base300 }} />
            <View style={{ gap: 12, paddingBottom: 16 }}>
                <Text type='h3' style={{ paddingHorizontal: 16 }}>Отзывы посетителей</Text>
                <View style={{ gap: 24 }}>
                    {
                        reviews.Count > 0 ?
                            reviews.Reviews.map((el, ind) => <Review key={el.ReviewGUID} review={el} page='eatery' onTrashPressed={() => 0} />)
                            :
                            <Text style={{ paddingHorizontal: 16 }}>У этого заведения еще нет отзывов</Text>
                    }
                </View>
                {/* <Button type='secondary' size='small' onPress={() => { }} style={{ marginHorizontal: 16 }}>Загрузить еще</Button> */}
            </View>
            {/* <View style={{ marginVertical: 24, marginHorizontal: 16, height: 1, backgroundColor: Colors.Base300 }} />
            <View>
                <Text type='h3' style={{ paddingHorizontal: 16, marginBottom: 4 }}>Лучшие блюда</Text>
                <Text type='sec1' state='secondary' style={{ paddingHorizontal: 16 }}>По оценкам посетителей</Text>
                <ScrollView contentContainerStyle={{ gap: 8, paddingHorizontal: 12, paddingVertical: 12, marginBottom: 24 }} showsHorizontalScrollIndicator={false} horizontal={true}>
                    {
                        //bestMeals.map(el => <MealComponent key={el.id} {...el} style={{ width: Dimensions.get('screen').width / 2.3 }} />)
                    }
                </ScrollView>
            </View> */}
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    headerPhoto: {
        width: 76,
        height: 76,
        borderRadius: 8,
    },

})
