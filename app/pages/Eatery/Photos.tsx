import React, { useEffect, useState } from 'react'
import { View, ScrollView, Image, Dimensions, StyleSheet, TouchableOpacity } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import { getEstablishment } from '../../api/establishment'
import { Eatery } from './Eatery'

import Text from '../../components/Text'
import Header from '../../components/Eatery/Header'
import ImageCarousel from '../../components/ImageCarousel'

import Icons from '../../components/Icons/Icons'


export default () => {
    const route = useRoute()
    const navigation = useNavigation<NativeStackNavigationProp<any>>()
    const [eatery, setEatery] = useState<Eatery | null>(null)
    const [imageView, setImageView] = useState({ imageIndex: 0, visible: false })

    const establishmentId = route.params.id

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Фотографии',
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerLeft: () => <Icons icon='ChevronLeft' color='default' size={24} padding={8} onPress={() => navigation.goBack()} />,
            headerTitle: ({ children }) => <Text type='h3'>{children}</Text>,
        })
    }, [navigation])

    const fetchData = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }
        await getEstablishment(establishmentId).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            setEatery(body)
        })
    }

    useEffect(() => {
        fetchData()
    }, [])

    if (eatery === null) {
        return <Text style={{ paddingHorizontal: 16 }}>Загрузка...</Text>
    }

    return (
        <ScrollView>
            <Header Establishment={eatery} />
            <ImageCarousel images={eatery.Images} {...imageView} onRequestClose={() => setImageView({ imageIndex: 0, visible: false })} />
            <View style={styles.photosContainer}>
                {
                    eatery.Images.map((el, ind) =>
                        <TouchableOpacity key={el} onPress={() => setImageView({ imageIndex: ind, visible: true })} style={ind % 3 == 0 && { width: '100%' }}>
                            <Image style={[styles.photo, ind % 3 == 0 && { width: '100%' }]} source={{ uri: el }} />
                        </TouchableOpacity>
                    )
                }
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    photosContainer: {
        paddingHorizontal: 16,
        paddingVertical: 24,
        gap: 8,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    photo: {
        width: (Dimensions.get('screen').width - 40) / 2,
        height: 200,
        borderRadius: 12,
    },
})
