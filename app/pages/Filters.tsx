import React, { useEffect, useState } from 'react'
import { StyleSheet, View } from 'react-native'
import Text from '../components/Text'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import Button from '../components/Button'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { getInputVars } from '../api/establishment'


export default () => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    const [vars, setVars] = useState<{ EstablishmentTypes: string[], DishCategories: string[], CuisineTypes: string[], SuitableCases: string[], AdditionalServices: string[] } | null>(null)

    const fetchVars = async () => {
        await getInputVars().then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            setVars(body)
        })
    }

    useEffect(() => {
        fetchVars()
    }, [])

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Фильтры',
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerBackVisible: false,
            headerLeft: () => <Button type='text' size='medium' onPress={() => 2}><Text state={'secondary'}>Сбросить</Text></Button>,
            headerRight: () => <Button type='text' size='medium' onPress={() => AsyncStorage.setItem('params', JSON.stringify({ types: ['Фастфуд'] })).then(() => navigation.goBack())}><Text state={'active'}>Применить</Text></Button>,
            headerTitle: ({ children }) => <Text type='h3'>{children}</Text>,
        })
    }, [navigation]);

    return (
        <View style={styles.container}>
            <Text type='h2'>Фильтры</Text>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 16,
        gap: 16,
    },
})
