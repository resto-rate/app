import React, { useEffect, useState } from 'react'
import { Image, ScrollView, StyleSheet, View } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import Text from '../components/Text'
import Icons from '../components/Icons/Icons'
import { EateryEvent } from './Eatery/Eatery'
import Chips from '../components/Chips'

import Colors from '../constants/Colors'


export default () => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()
    const [event, setEvent] = useState<EateryEvent | null>(null)

    useEffect(() => {
        AsyncStorage.getItem('event').then(event => {
            if (event) {
                setEvent(JSON.parse(event))
            }
        })
    }, [])

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Мероприятие',
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerTitle: ({ children }) => <Text type='h3'>{children}</Text>,
            headerRight: () => <Icons icon='Cross' color='default' size={24} padding={8} onPress={() => navigation.goBack()} />,
        });
    }, [navigation, event]);

    if (event === null) {
        return <></>
    }

    const date = new Date(event.DateTime)

    return (
        <ScrollView style={styles.container}>
            <Image source={{ uri: event.Image.length > 0 ? event.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/event_placeholder.jpg' }} style={styles.image} />
            <View style={styles.info}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text type='sec1' state='primary'>{date.toLocaleDateString()} в {date.toLocaleTimeString()}</Text>
                    <Chips color='black'>{event.EstablishmentTitle}</Chips>
                </View>
                <Text type='h3' state='primary' style={{ borderBottomWidth: 1, borderColor: Colors.Divider }}>{event.Title}</Text>
                <Text type='sec1'>{event.Description}</Text>
                <Text type='sec1' state='primary'>Подробнее: {event.ContactInfo}</Text>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        marginHorizontal: 32,
        aspectRatio: 9 / 16,
        marginTop: 12,
        marginBottom: 24,
        borderRadius: 8,
    },
    info: {
        gap: 12,
        paddingHorizontal: 16,
    },
})
