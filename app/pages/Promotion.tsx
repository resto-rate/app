import React, { useEffect, useState } from 'react'
import { Image, ScrollView, StyleSheet, View } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { prettyDate } from '../utils/Functions'
import { EateryPromotion } from './Eatery/Eatery'

import Text from '../components/Text'
import Icons from '../components/Icons/Icons'

import Colors from '../constants/Colors'


export default () => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()
    const [promotion, setPromotion] = useState<EateryPromotion | null>(null)

    useEffect(() => {
        AsyncStorage.getItem('promo').then((promo) => {
            if (promo) {
                setPromotion(JSON.parse(promo))
            }
        })
    }, [])

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Акция',
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerTitle: ({ children }) => <Text type='h3'>{children}</Text>,
            headerRight: () => <Icons icon='Cross' color='default' size={24} padding={8} onPress={() => navigation.goBack()} />,
        });
    }, [navigation, promotion]);

    if (promotion === null) {
        return <></>
    }

    return (
        <ScrollView style={styles.container}>
            <Image source={{ uri: promotion.Image.length > 0 ? promotion.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/promotion_placeholder.jpg' }} style={styles.image} />
            <View style={styles.info}>
                <Text type='h3' state='primary' style={{ borderBottomWidth: 1, borderColor: Colors.Divider }}>{promotion.Title}</Text>
                <Text type='sec1'>{promotion.Conditions}</Text>
                <Text type='sec1' state='primary'>Период действия: {promotion.Undying ? 'не ограничен' : `с ${prettyDate(promotion.StartTime)} по ${prettyDate(promotion.EndTime)}`}</Text>
                <Text type='sec1' state='primary'>Подробнее: {promotion.ContactInfo}</Text>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 16,
    },
    image: {
        marginHorizontal: 32,
        aspectRatio: 9 / 16,
        marginTop: 12,
        marginBottom: 24,
        borderRadius: 8,
    },
    info: {
        gap: 12,
        paddingHorizontal: 16,
    },
})
