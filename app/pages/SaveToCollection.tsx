import React, { useState } from 'react'
import { Image, ScrollView, StyleSheet, View } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import { useUserContext } from '../context/userContext'
import { Collection as CollectionType, useCollectionsContext } from '../context/collectionsContext'

import Text from '../components/Text'
import Icons from '../components/Icons/Icons'
import Input from '../components/Input'

import Emptybox from '../assets/images/emptybox.svg'
import Collection from '../components/Profile/Collection'
import Button from '../components/Button'
import { addToCollection } from '../api/establishment'

export default () => {
    const route = useRoute()
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    const { user } = useUserContext()
    const { collections, setCollections } = useCollectionsContext()

    const [query, setQuery] = useState('')

    const eateryId = route.params.id

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Сохранить в подборку',
            headerBackVisible: false,
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerTitle: ({ children }) => <Text type='h3' state='active' style={{ flex: 1 }}>{children}</Text>,
            headerRight: () => <Icons icon='Cross' color='default' size={24} padding={8} onPress={() => navigation.goBack()} />,
        });
    }, [navigation])

    const saveEatery = (CollectionGUID: string) => {
        addToCollection({ EstablishmentGUID: eateryId }, CollectionGUID, user.At.Token).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json()
            }
        }).then((body: CollectionType) => {
            if (body) {
                navigation.goBack()
            }
        })
    }

    return (
        <ScrollView style={styles.container}>
            <Input value={query} onChangeText={setQuery} placeholder='Поиск по подборкам' />
            <View>
                {
                    collections.Count > 0 ?
                        <View style={styles.collections}>
                            {collections.Collections.map((el) => <Collection onPress={saveEatery} key={el.CollectionGUID} {...el} />)}
                        </View>
                        :
                        <View style={styles.placeholder}>
                            <Emptybox width={140} height={140} />
                            <Text type='h2'>Пока пусто</Text>
                            <Text type='sec1' state='secondary'>Здесь будут Ваши коллекции заведений</Text>
                        </View>
                }
            </View>
            <Button type='primary' size='large' onPress={() => 0}>Новая подборка</Button>
        </ScrollView >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
        gap: 24,
    },
    placeholder: {
        marginVertical: '20%',
        alignItems: 'center',
        gap: 8,
    },
    collections: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        gap: 8,
        rowGap: 24,
        marginVertical: 16,
    },
})
