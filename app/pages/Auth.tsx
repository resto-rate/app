import React, { useState, useEffect } from 'react'
import { View, StyleSheet, TextInput, Platform, ToastAndroid, Alert, KeyboardAvoidingView } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

import { changeProfileData, getOtpCode, getProfile, loginOtp, notificationRegister } from '../api/auth'
import { useUserContext } from '../context/userContext'
import { capitalizeFirst } from '../utils/Functions'

import Text from '../components/Text'
import Input from '../components/Input'
import Button from '../components/Button'
import OTP from '../components/Auth/OTP'

import Icons from '../components/Icons/Icons'

import Colors from '../constants/Colors'


export default () => {
    const navigation = useNavigation<NativeStackNavigationProp<any>>()

    const { user, setUser } = useUserContext()
    const [stage, setStage] = useState('phone')

    const [phone, setPhone] = useState('')
    const [otpGUID, setOtpGUID] = useState('')
    const [retrySendOTPSeconds, setRetrySendOTPSeconds] = useState(0)
    const [name, setName] = useState('')
    const [surname, setSurname] = useState('')

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Вход',
            headerShadowVisible: false,
            headerTitleAlign: 'center',
            headerBackVisible: false,
            headerLeft: () => stage.substring(0, 3) === 'otp' ? <Icons icon='ChevronLeft' color='default' size={24} padding={8} onPress={() => previousPage()} /> : <></>,
            headerRight: () => stage.substring(0, 4) !== 'name' ? <Icons icon='Cross' color='default' size={24} padding={8} onPress={() => navigation.goBack()} /> : <></>,
            headerTitle: ({ children }) => <Text type='h3'>{children}</Text>,
        });
    }, [navigation, stage]);

    const previousPage = () => {
        switch (stage) {
            case 'otp':
            case 'otpLoading':
                setStage('phone')
                return
        }
    }

    const sendPhone = async () => {
        setStage('phoneLoading')
        if (phone.length !== 10) {
            setStage('phone')
            return
        }
        await getOtpCode({ Login: phone }).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
            setStage('phone')
            switch (resp.status) {
                case 400:
                    if (Platform.OS === 'ios') {
                        Alert.alert('Ошибка', 'Слишком много попыток, попробуйте ещё раз через минуту')
                    } else {
                        ToastAndroid.showWithGravity('Слишком много попыток, попробуйте ещё раз через минуту', ToastAndroid.LONG, ToastAndroid.CENTER)
                    }
                    setStage('phone')
                    return
                default:
                    Alert.alert(`Произошла неизвестная ошибка, код ${resp.status}`)
                    break
            }
        }).then(body => {
            if (body) {
                setOtpGUID(body.OtpGUID)
                setRetrySendOTPSeconds(body.AuthExpiredSeconds)
                setStage('otp')
            }
        }).catch(resp => {
            setStage('phone')
        })
    }

    const sendOtp = async (code: string) => {
        setStage('otpLoading')
        await loginOtp({ Login: phone, OTP: code, OtpGUID: otpGUID }).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
            switch (resp.status) {
                case 400:
                    if (Platform.OS === 'ios') {
                        Alert.alert('Ошибка', 'Неправильный код')
                    } else if (Platform.OS === 'android') {
                        ToastAndroid.showWithGravity('Неправильный код', ToastAndroid.LONG, ToastAndroid.CENTER)
                    }
                    setStage('otp')
                    return
                case 404:
                    if (Platform.OS === 'ios') {
                        Alert.alert('Ошибка', 'Код устарел')
                    } else if (Platform.OS === 'android') {
                        ToastAndroid.showWithGravity('Код устарел', ToastAndroid.LONG, ToastAndroid.CENTER)
                    }
                    setStage('otp')
                    return
                default:
                    Alert.alert('Ошибка', `Произошла неизвестная ошибка, код ${resp.status}`)
                    break
            }
            setStage('otp')
        }).then(body => {
            if (body) {
                const tmpUser = { ...user, ...body }
                AsyncStorage.setItem('user', JSON.stringify(tmpUser)).then(() => {
                    AsyncStorage.getItem('firebase token').then(storedToken => {
                        if (storedToken) {
                            notificationRegister({ Token: storedToken, AccountGUID: tmpUser.AccountGUID })
                        }
                    })
                })
                if (body.AccountStatus == 1) {
                    setStage('name')
                    setUser(tmpUser)
                } else {
                    getProfile(body.At.Token).then(resp => {
                        return resp.json()
                    }).then(body => {
                        setUser({ ...user, ...tmpUser, ...body, Empty: false })
                        navigation.goBack()
                    })
                }
            }
        }).catch(err => {
            console.log(err)
        })
    }

    const sendNewUserData = async () => {
        setStage('nameLoading')
        if (!name || !surname) {
            console.log('Имя и фамилия не могут быть пустыми!')
            setStage('name')
            return
        }
        await changeProfileData({ Name: capitalizeFirst(name.replace(/\s/g, "")), Surname: capitalizeFirst(surname.replace(/\s/g, "")), City: 'Нижний Новгород' }, user.At.Token).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
            setStage('name')
            return resp.json()
        }).then(body => {
            if (body) {
                setUser({ ...user, ...body, Empty: false })
                navigation.goBack()
            }
        }).catch(err => {
            console.log(err)
        })
    }

    useEffect(() => {
        retrySendOTPSeconds > 0 && setTimeout(() => setRetrySendOTPSeconds(retrySendOTPSeconds - 1), 1000)
    }, [retrySendOTPSeconds])

    return (
        <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === 'ios' ? 'padding' : 'height'} keyboardVerticalOffset={Platform.OS === "android" ? 100 : 0}>
            <Text type='h1' style={styles.heading}>{stage === 'phone' || stage == 'phoneLoading' ? 'Войти или зарегистрироваться' : stage === 'otp' || stage === 'otpLoading' ? 'Введите код' : 'Давайте знакомиться'}</Text>
            {
                stage === 'phone' || stage == 'phoneLoading' ?
                    <>
                        <View style={styles.phoneContainer}>
                            <Text type='sec1'>Номер телефона</Text>
                            <View style={styles.phoneInputContainer}>
                                <Text>+7</Text>
                                <TextInput value={phone} onChangeText={setPhone} style={styles.phoneInput} textContentType={'telephoneNumber'} keyboardType={'phone-pad'} inputMode={'tel'} autoFocus />
                            </View>
                            <Text type='sec1'>На указанный номер телефона придет код подтверждения</Text>
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button type='primary' size='large' onPress={sendPhone} loading={stage == 'phoneLoading'} disabled={phone.length !== 10}>Продолжить</Button>
                        </View>
                    </>
                    :
                    stage === 'otp' || stage == 'otpLoading' ?
                        <>
                            <View>
                                <OTP onFilled={sendOtp} style={styles.otpInput} />
                                {
                                    retrySendOTPSeconds > 0 ?
                                        <Text type='sec1'>Устареет через {retrySendOTPSeconds} c.</Text>
                                        :
                                        <Button type='secondary' size='medium' onPress={sendPhone}>Отправить код повторно</Button>
                                }
                            </View>
                        </>
                        :
                        <>
                            <View style={styles.nameContainer}>
                                <Input value={name} onChangeText={setName} placeholder='Имя' autoFocus />
                                <Input value={surname} onChangeText={setSurname} placeholder='Фамилия' />
                            </View>
                            <View style={styles.buttonContainer}>
                                <Button type='primary' size='large' onPress={sendNewUserData} loading={stage == 'nameLoading'} disabled={!name.length || !surname.length}>Сохранить</Button>
                            </View>
                        </>
            }
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 16,
        paddingBottom: 32,
    },
    heading: {
        marginVertical: 24,
    },
    phoneContainer: {
        gap: 8,
        marginBottom: 16,
    },
    phoneInputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    phoneInput: {
        borderBottomWidth: 1,
        borderBottomColor: Colors.Base900,
        color: Colors.Base900,
        paddingHorizontal: 4,
        paddingVertical: 8,
        fontFamily: 'Manrope400',
        fontSize: 16,
        flex: 1,
    },
    otpInput: {
        marginBottom: 20,
    },
    buttonContainer: {
        flex: 1,
        position: 'relative',
        flexDirection: 'column-reverse',
    },
    nameContainer: {
        gap: 20,
        marginBottom: 24,
    },
})
