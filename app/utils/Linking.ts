const config = {
    initialRouteName: 'Tabs',
    screens: {
        Tabs: {
            path: '*',
        },
        Eatery: {
            path: 'rest/:id',
        },
        Collection: {
            path: 'collection/:id'
        },
    },
}

export default {
    prefixes: ['restorate://', 'https://web.restorate.space', 'https://web.restorate.space'],
    config,
}
