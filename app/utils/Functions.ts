import { Alert, Linking, Platform, Share } from "react-native"

export const switchCriterion: (criterion: string) => 'Обслуживание' | 'Питание' | 'Атмосфера' | 'Цена - качество' | 'Время ожидания' = (criterion) => {
    switch (criterion) {
        case 'service':
            return 'Обслуживание'
        case 'vibe':
            return 'Атмосфера'
        case 'food':
            return 'Питание'
        case 'waitingtime':
            return 'Время ожидания'
        case 'pricequality':
            return 'Цена - качество'
        default:
            return 'Обслуживание'
    }
}

export const capitalizeFirst = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

export const prettyPhone: (phone: string) => string = phone => {
    return '8 (' + phone.substring(0, 3) + ') ' + phone.substring(3, 6) + '-' + phone.substring(6, 8) + '-' + phone.substring(8, 10)
}

export const prettyTime: (time: number) => string = time => {
    return new Date(time * 1000).toJSON().substring(11, 16)
}

export const prettyDate: (date: number) => string = date => {
    return new Date(date).toLocaleDateString()
}

export const openGps = (Geolocation: { Latitude: number, Longitude: number }, EstablishmentTitle: string, Address: string) => {
    const url = `yandexmaps://maps.yandex.ru/?pt=${Geolocation.Longitude},${Geolocation.Latitude}&z=15`
    Linking.openURL(url).catch(e => {
        if (Platform.OS == 'ios') {
            Linking.openURL(`maps://${Geolocation.Latitude},${Geolocation.Longitude}`)
        } else {
            Linking.openURL(`geo:0,0?q=${encodeURI(EstablishmentTitle)}@${encodeURI(Address)}&z=15`)
        }
    })
}

export const encodeURISearchParams = (params: object) => '&' + Object.entries(params).map(([key, value]) => `${key}=${encodeURI(value)}`).join('&')

export const shareText = async (text: string) => {
    try {
        const result = await Share.share({ message: text })
        if (result.action === Share.sharedAction) {
            if (result.activityType) {
                // shared with activity type of result.activityType
            } else {
                // shared
            }
        } else if (result.action === Share.dismissedAction) {
            // dismissed
        }
    } catch (error: any) {
        Alert.alert(error.message)
    }
}

export const switchEnding = (forms: string[], number: number) => {
    if (11 <= number % 100 && number % 100 <= 19 || 0 === number % 10 || 5 <= number % 10) {
        return forms[0]
    }
    if (2 <= number % 10 && number % 10 <= 5) {
        return forms[2]
    }
    return forms[1]
}
